import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from "src/environments/environment";
import Swal from 'sweetalert2';
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  profilePic: string;
  url: any;
  viewData: any;
  _fileData: File = null;
  constructor(private auth: AuthService,
    private spinner:NgxSpinnerService,
    private toaster: ToastrService,
    private router:Router) {}

  ngOnInit(): void {
    this.auth.getProfessionalInfo().subscribe((data) => {
      if (data.data.profilePic != null) {
        this.url = data.data.profilePic;
        this.viewData = data.data;
        this.getProfilePic();
      } else {
        if (data.data.genderId) {
        }
      }
    });
  }
  showProfilePic(src: any) {
    this.profilePic = src;
  }
  getProfilePic() {
    this.profilePic = environment.baseImgUrl + this.url;
  }

  changeProfilePic(fileInput: any) {
    this._fileData = <File>fileInput.target.files[0];
    this.onUpdateProfilePic();
  }
  onUpdateProfilePic() {
    const formData = new FormData();
    formData.append("imgFile", this._fileData, this._fileData.name);
    this.auth.updateProfilePic(formData).subscribe((res) => {
      if (res.status) {
        localStorage.setItem("profilePic", res.data.profilepicurl);

        this.profilePic =
          environment.baseImgUrl + localStorage.getItem("profilePic");
      }
    });
  }

logout() {
  Swal.fire({
  title: "Are you sure you want to sign out?",
  icon: "question",
  showCancelButton: true,
  confirmButtonColor: "#3085d6",
  cancelButtonColor: "#d33",
  allowOutsideClick: false,
  backdrop: `
  rgba(0, 0, 0, 0.7)
  left top
  no-repeat
  `,
  cancelButtonText: "No",
  confirmButtonText: "Yes",
  }).then((result) => {
  if (result.isConfirmed) {
  this.spinner.show();
  this.auth.logout().subscribe((res) => {
  if (res.status) {
  this.router.navigateByUrl("/login");
  this.spinner.hide();
  this.toaster.success(res.message);
  } else {
  this.spinner.hide();
  this.toaster.error(res.message);
  }
  });
  }
  });
  }
}
