import { NgModule } from "@angular/core";
import { AlphabetOnlyDirective } from "./directives/alphabet-only.directive";

import { OnlyNumberDirective } from "./directives/only-number.directive";

@NgModule({
  declarations: [OnlyNumberDirective, AlphabetOnlyDirective],
  imports: [],
  providers: [],
  exports: [OnlyNumberDirective, AlphabetOnlyDirective],
})
export class SharedModule {}
