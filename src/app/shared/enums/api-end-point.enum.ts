export enum ApiEndPoint {
  login = "Auth/PatientLogin",
  register = "Auth/PatientRegister",
  forgotPassword = "Auth/ForgotPassword",
  resetPassword = "Auth/ResetPassword",
  googleLogin = "Auth/PatientSocialLogin",
  facebookLogin = "Auth/PatientSocialLogin",
  changePassword = "Auth/ChangePassword",
  verifyEmail = "Auth/VerifyEmail",
  resendEmailCode = "Auth/ResendEmailCode",
  VerifyPhone = "Auth/VerifyPhone",
  ResendPhoneOtp = "Auth/ResendPhoneOtp",
  getPatientBasicInfo = "Patient/GetPatientBasicInfo",
  getPatientAdditionalInfo = "Patient/GetPatientAdditionalInfo",
  patientHealthInfo = "Patient/AddPatientHealthInfo",
  updatePatientBasicInfo = "Patient/UpdatePatientBasicInfo",
  getPatientDashboardInfo = "Patient/GetPatientDashboardInfo",
  addPrescriptions = "Prescription/AddPrescriptions",
  addPatientTest = "Prescription/AddPatientTest",

  getBlogs = "Blog/GetBlogList",
  deleteBlog = "Blog/DeleteBlog",
  getBlogDetail = "Blog/GetBlogDetail",
  getBlogList = "Blog/GetBlogList",
  getAllBlogs = "Blog/GetAllBlogs",
  getUserBlogs = "Blog/GetUserBlogs",
  approveBlogStatus = "Blog/ApproveBlogStatus",

  updateProfilePic = "Auth/UpdateProfilePic",
  updatePatientProfile = "Auth/UpdatePatientProfile",

  getCountries = "Content/GetCountries",
  getStates = "Content/GetStates",

  postMedicalHistory = "MedicalHistory/AddPastMedicalHistory",
  getPostMedicalHistory = "MedicalHistory/GetPastMedicalHistory",
  deletePostMedicalList = "MedicalHistory/DeletePastMedicalHistory",

  SurgicalHistory = "MedicalHistory/AddPatientSurgicalHistory",
  surgicalHistoryList = "MedicalHistory/GetPatientSurgicalHistory",
  deleteSurgicalHistoryList = "MedicalHistory/DeleteSurgicalHistory",

  allergyHistory = "MedicalHistory/AddPatientAllergyHistory",
  allergyHistoryList = "MedicalHistory/GetPatientAllergyHistory",
  deleteAllergyList = "MedicalHistory/DeleteAllergyHistory",

  familyHistory = "MedicalHistory/AddPatientFamilyHistory",
  familyHistoryList = "MedicalHistory/GetPatientFamilyHistory",
  deleteFamilyList = "MedicalHistory/DeleteFamilyHistory",

  postAttachmentHistory = "MedicalHistory/AddPatientMedicalReport",
  getAttachmentHistoryList = "MedicalHistory/GetPatientMedicalReport",
  deleteAttachmentList = "MedicalHistory/DeletePatientMedicalReport",

  contactUs = "Content/ContactUs",

  postNotifications = "Content/UpdateNotificationStatus",

  getSetting = "Content/GetAppSettings",

  getDoctorTopList = "Doctor/GetTopDoctors",

  getAllDoctorList = "Doctor/GetAllDoctorList",

  getDoctorInfo = "Doctor/GetDoctorDescription",

  getPrescriptionList = "Prescription/GetPrescriptionList",

  GetSpeciality = "Content/GetSpeciality",

  GetAvailableDatesForPatient = "Schedule/GetAvailableDatesForPatient",

  getTimeSlotsForPatient = "Schedule/GetTimeSlotsForPatient",
  bookingAppointment = "Appointment/BookAppointment",

  getPatientUpcomingAppointments = "Appointment/GetPatientUpcomingAppointments",
  updateAppointmentStatus = "Appointment/UpdateAppointmentStatus",
  getPatientPastAppointments = "Appointment/GetPatientPastAppointments",

  getUserReview = "Rating/GetUserReviews",

  addUpdateUserRating = "Rating/AddUpdateUserRating",

  getVideoCallToken = "Media/GetVideoCallToken",
  logout = "Auth/Logout",
}
