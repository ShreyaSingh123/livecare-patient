import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "../services/auth.service";
import Swal from "sweetalert2";
import { Router } from "@angular/router";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private toaster: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const currentUser = this.authService.currentUserValue;
    if (currentUser) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.data.accessToken}`,
        },
      });
    }
    return next.handle(request).pipe(
      tap(
        (event) => {
          if (event instanceof HttpResponse) {
          }
        },
        (error) => {
          switch (error.status) {
            case 401: {
              this.spinner.hide();
              Swal.fire({
                title: "",
                text:
                  "Your Session has been expired as you are logged in from another device",
                icon: "warning",
                showCancelButton: false,
                confirmButtonColor: "#3085d6",
                allowOutsideClick: false,
                cancelButtonColor: "#d33",
                confirmButtonText: "Ok",
                backdrop: `
rgba(0, 0, 0, 0.8)
left top
no-repeat
`,
              }).then((result) => {
                if (result.isConfirmed) {
                  this.authService.logout();
                  localStorage.removeItem("currentUser");
                  this.router.navigate(["login"]);
                }
              });
              break;
            }
            case 500: {
              this.spinner.hide();
              this.toaster.error("Internal Server Error");
              break;
            }
            case 0: {
              this.spinner.hide();
              this.toaster.error("Please Check Your Internet Connection");
              break;
            }
          }
        }
      )
    );
  }
}
