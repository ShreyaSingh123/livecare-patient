// import { Injectable } from "@angular/core";
// import {
//   CanActivate,
//   ActivatedRouteSnapshot,
//   RouterStateSnapshot,
//   UrlTree,
//   Router,
// } from "@angular/router";
// import { Observable } from "rxjs";
// import { AuthService } from "../services/auth.service";

// @Injectable({
//   providedIn: "root",
// })
// export class AuthGuard implements CanActivate {
//   constructor(private router: Router, private authService: AuthService) {}
//   canActivate() {
//     const currentUser = this.authService.currentUserValue;
//     if (currentUser) {
//       // logged in so return true
//       return true;
//     }

//     // not logged in so redirect to login page with the return url

//     this.router.navigate(["/login"]);
//     return false;
//   }
// }

import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, private myRoute: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.isLoggedIn()) {
      return true;
    } else {
      this.myRoute.navigate(["login"]);
      return false;
    }
  }
}
