import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ApiEndPoint } from '../enums/api-end-point.enum';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PrescriptionService {

  constructor(private http: HttpClient, private router: Router) {

  }


  getPrescriptionList() {
    return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getPrescriptionList).pipe(
      map((data: any) => {
        return data;
      })
    );
  }
}
