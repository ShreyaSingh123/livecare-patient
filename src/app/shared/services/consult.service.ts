import { Injectable } from "@angular/core";

import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../enums/api-end-point.enum";
import { map } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class ConsultServices {
  // appointmentStatus = new BehaviorSubject<any>(0);
  constructor(private http: HttpClient, private router: Router) {}

  // sendAppointmentId(data:any){
  //   this.appointmentStatus.next(data)
  // }
  upcomingAppointments(data) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getPatientUpcomingAppointments +
        "?appointmentStatus=" +
        data
    );
  }

  UpdateAppointmentStatus(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateAppointmentStatus,
      data
    );
  }

  pastAppointments(pageNumber: any, pageSize: any) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getPatientPastAppointments +
        "?pageNumber=" +
        pageNumber +
        "&pageSize=" +
        pageSize
    );
  }
}
