import { Injectable } from "@angular/core";

import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../enums/api-end-point.enum";
import { map } from "rxjs/operators";
import * as OT from "@opentok/client";
@Injectable({
  providedIn: "root",
})
export class OpenTokService {
  session: OT.Session;
  token: string;
  appointments: any;

  constructor(private http: HttpClient, private router: Router) {}

  getOT() {
    return OT;
  }

  initCallSession(token: any, sessionId: any) {
    debugger;
    this.appointments = JSON.parse(localStorage.getItem("appointments"));
    if (
      this.appointments.data.openTokApiKey &&
      this.appointments.data.mediaValue.token &&
      this.appointments.data.mediaValue.sessionId
    ) {
      this.session = this.getOT().initSession(
        this.appointments.data.openTokApiKey,
        this.appointments.data.mediaValue.sessionId
      );
      this.token = token;
      return Promise.resolve(this.session);
    } else {
      return fetch(environment.apiUrl + "/session")
        .then((data) => data.json())
        .then((json) => {
          this.session = this.getOT().initSession(json.apiKey, json.sessionId);
          this.token = json.token;
          return this.session;
        });
    }
  }

  initSession() {
    if (
      this.appointments.data.openTokApiKey &&
      this.appointments.data.mediaValue.token &&
      this.appointments.data.mediaValue.sessionId
    ) {
      this.session = this.getOT().initSession(
        this.appointments.data.openTokApiKey,
        this.appointments.data.mediaValue.sessionId
      );
      this.token = this.appointments.data.openTokApiKey;
      return Promise.resolve(this.session);
    } else {
      return fetch(environment.apiUrl + "/session")
        .then((data) => data.json())
        .then((json) => {
          this.session = this.getOT().initSession(json.apiKey, json.sessionId);
          this.token = json.token;
          return this.session;
        });
    }
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.session.connect(this.token, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(this.session);
        }
      });
    });
  }

  disconnect() {
    return new Promise<void>((resolve, reject) => {
      this.session.disconnect();
    });
  }

  getVideoCallToken(AppointmentId: any) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getVideoCallToken +
          "?AppointmentId=" +
          AppointmentId
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }
}
