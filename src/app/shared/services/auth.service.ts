import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../enums/api-end-point.enum";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { map } from "rxjs/operators";
import { LoginComponent } from "src/app/auth/login/login.component";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  postedSource = new BehaviorSubject<any>(0);
  forgetEmail: any;
  verifiedPhone = new BehaviorSubject<any>(0);

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<LoginComponent>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
    this.forgetEmail = this.postedSource.asObservable();
  }
  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  sharePostedData(email: any) {
    this.postedSource.next(email);
  }

  // sendVerifiedPhone(data: any) {
  //   this.verifiedPhone.next(data);
  // }

  // Patient Login
  login(data: any) {
    return this.http
      .post<any>(environment.apiUrl + "/" + ApiEndPoint.login, data)
      .pipe(
        map((user) => {
          // var data = user;
          // data.data["t"] = 9;

          if (user.status) {
            localStorage.setItem("currentUser", JSON.stringify(user));
            this.currentUserSubject.next(user);
          } else {
            if (user.data.isEmailVerified == false) {
            } else {
              this.router.navigateByUrl("/login");
            }
          }

          return user;
        })
      );
  }

  getToken() {
    return localStorage.getItem("currentUser");
  }
  isLoggedIn() {
    return this.getToken() !== null;
  }

  // update patient profile

  updatePatientProfile(data: any) {
    var headers_object = new HttpHeaders().set(
      "Authorization",
      "Bearer " + localStorage.getItem("accessToken")
    );
    return this.http
      .post<any>(
        environment.apiUrl + "/" + ApiEndPoint.updatePatientProfile,
        data,
        { headers: headers_object }
      )
      .pipe(
        map((user) => {
          var data = user;
          data.data["accessToken"] = localStorage.getItem("accessToken");
          if (data.status) {
            localStorage.setItem("currentUser", JSON.stringify(data));
            this.currentUserSubject.next(data);
          } else {
            this.router.navigateByUrl("/login");
          }

          return user;
        })
      );
  }

  // Patient Register
  register(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.register,
      data
    );
    // .pipe(
    //   map((user) => {
    //     if (user.status) {
    //       localStorage.setItem("currentUser", JSON.stringify(user));
    //       this.currentUserSubject.next(user);
    //     } else {
    //       this.router.navigateByUrl("/register");
    //     }
    //     return user;
    //   })
    // );
  }

  // Patient Forgot-Password
  forgotPassword(email: string) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.forgotPassword,
      email
    );
  }

  // Reset Password
  onResetPassword(resetPassword: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.resetPassword,
      resetPassword
    );
  }

  googleLogin(googleLoginDetials: any) {
    return this.http
      .post<any>(
        environment.apiUrl + "/" + ApiEndPoint.googleLogin,
        googleLoginDetials
      )
      .pipe(
        map((user: any) => {
          if (user.status) {
            localStorage.setItem("currentUser", JSON.stringify(user));
            this.currentUserSubject.next(user);
          } else {
            this.router.navigateByUrl("/login");
          }
          return user;
        })
      );
  }

  facebookLogin(facebookLoginDetials: any) {
    return this.http
      .post<any>(
        environment.apiUrl + "/" + ApiEndPoint.facebookLogin,
        facebookLoginDetials
      )
      .pipe(
        map((user: any) => {
          if (user.status) {
            localStorage.setItem("currentUser", JSON.stringify(user));
            this.currentUserSubject.next(user);
          } else {
            this.router.navigateByUrl("/login");
          }
          return user;
        })
      );
  }

  // On Patient Logout
  logout() {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.logout,
      null
    );
  }

  //On change password
  onChangePassword(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.changePassword,
      data
    );
  }

  //On verify email

  onVerifyEmail(data: any) {
    var headers_object = new HttpHeaders().set(
      "Authorization",
      "Bearer " + localStorage.getItem("accessToken")
    );
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.verifyEmail,
      data,
      { headers: headers_object }
    );
  }

  //On resend email
  resendEmailCode(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.resendEmailCode,
      data
    );
  }

  // //On verify phoneNumber
  VerifyPhoneNumber(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.VerifyPhone,
      data
    );
  }

  // //on resend phonenumber
  ResendPhoneOtp(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.ResendPhoneOtp,
      data
    );
  }

  //Update Profile pic
  updateProfilePic(imgFile: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateProfilePic,
      imgFile
    );
  }

  //update patientBasicInfo
  updatePatientBasicInfo(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updatePatientBasicInfo,
      data
    );
  }

  //update getPatientAdditionalInfo
  patientHealthInfo(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.patientHealthInfo,
      data
    );
  }
  //get PatientAdditionalInfo
  PatientAdditionalInfo() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getPatientAdditionalInfo
    );
  }
  //getProfessional
  getProfessionalInfo() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getPatientBasicInfo)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getPatientDashboardInfo() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getPatientDashboardInfo
    );
  }
  // get countryList
  getCountries() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getCountries)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //get StatesList
  getStatesById(id: number) {
    return this.http
      .get<any>(
        environment.apiUrl + "/" + ApiEndPoint.getStates + "?CountryId=" + id
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //post PastMedicalHistory
  pastHistory(data) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.postMedicalHistory,
      data
    );
  }

  //get PastMedicalHistory
  getPastMedicalHistory() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getPostMedicalHistory)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //delete PastMedicalList
  deletePastMedicalList(id: any) {
    return this.http.delete<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.deletePostMedicalList +
        "?MedicalHistoryId=" +
        id
    );
  }

  //post SurgicalHistory
  surgicalHistory(data) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.SurgicalHistory,
      data
    );
  }
  //getAllergyList
  getSurgicalHistoryList() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.surgicalHistoryList)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //delete SurgicalMedicalList
  deleteSurgicalMedicalList(id: any) {
    return this.http.delete<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.deleteSurgicalHistoryList +
        "?surgicalHistoryId=" +
        id
    );
  }

  //post AllergyHistory
  postAllergyHistory(data) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.allergyHistory,
      data
    );
  }

  //getAllergyList
  getAllergyHistoryList() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.allergyHistoryList)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //delete AllergyListList
  deleteAllergyList(id: any) {
    return this.http.delete<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.deleteAllergyList +
        "?allergyHistoryId=" +
        id
    );
  }

  //postFamilyHistory
  postFamilyHistory(data) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.familyHistory,
      data
    );
  }

  //getFamilyHistoryList
  getFamilyHistoryList() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.familyHistoryList)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //delete Family List
  deleteFamilyList(id: any) {
    return this.http.delete<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.deleteFamilyList +
        "?familyHistoryId=" +
        id
    );
  }

  //postAttachment
  postAttachment(data, MedicalDocument) {
    var form = new FormData();
    form.append("ReportName", data.ReportName);
    form.append("Date", data.Date);
    form.append("Description", data.Description);
    form.append("MedicalDocument", MedicalDocument);
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.postAttachmentHistory,
      form
    );
  }

  //get AttachmentHistory
  getAttachmentHistory() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getAttachmentHistoryList)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //delete Family List
  deleteAttachmentList(id: any) {
    return this.http.delete<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.deleteAttachmentList +
        "?medicalReportId=" +
        id
    );
  }

  //contactUs
  contactUs(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.contactUs,
      data
    );
  }

  //getBlogsList
  getBlogList(pageNumber: any, pageSize: any) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getBlogs +
          "?pageNumber=" +
          pageNumber +
          "&pageSize=" +
          pageSize
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //get Blogs Details
  getBlogDetails(id) {
    return this.http
      .get<any>(
        environment.apiUrl + "/" + ApiEndPoint.getBlogDetail + "?blogId=" + id
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //Email notications
  postNotifications(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.postNotifications,
      data
    );
  }

  //getSetting
  getSetting() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getSetting)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }
}
