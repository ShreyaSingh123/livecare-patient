import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ApiEndPoint } from '../enums/api-end-point.enum';
import { map } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class UserReviewRatingService {


  constructor(private http: HttpClient, private router: Router,
    ) {

  }
  getUserReview(id) {
    return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getUserReview + "?DoctorId=" + id).pipe(
      map((data: any) => {
        return data;
      })
    );
  }
  updateUserRating(data:any){
  return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addUpdateUserRating, data)
}

}
