import { Injectable } from "@angular/core";

import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../enums/api-end-point.enum";
import { map } from "rxjs/operators";
import { Subject, BehaviorSubject } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class DoctorListService {
  constructor(private http: HttpClient, private router: Router) {}

  //getDoctorTopList
  getDoctorTopList(pageNumber, pageSize) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getDoctorTopList +
          "?pageNumber=" +
          pageNumber +
          "&pageSize=" +
          pageSize
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getAllDoctorList(data: any) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getAllDoctorList +
          "?pageNumber=" +
          data.pageNumber +
          "&pageSize=" +
          data.pageSize +
          "&sortOrder=" +
          data.sortOrder +
          "&sortField=" +
          data.sortField +
          "&searchQuery=" +
          data.searchQuery +
          "&filterBy=" +
          data.filterBy
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getSpeciality() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.GetSpeciality
    );
  }

  getSpecialityDetails(id, pageSize, pageNumber) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getAllDoctorList +
        "?filterBy=" +
        id +
        "&pageSize=" +
        pageSize +
        "&pageNumber=" +
        pageNumber
    );
  }

  getDoctorInfo(id) {
    return this.http
      .get<any>(
        environment.apiUrl + "/" + ApiEndPoint.getDoctorInfo + "?DoctorId=" + id
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getAvailableDatesForPatient(doctorId, date) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.GetAvailableDatesForPatient +
          "?DoctorId=" +
          doctorId +
          "&Date=" +
          date
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getTimeSlotsForPatient(doctorId, selectedDate) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getTimeSlotsForPatient +
        "?DoctorId=" +
        doctorId +
        "&selectedDate=" +
        selectedDate
    );
  }

  bookAppointment(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.bookingAppointment,
      data
    );
  }
}
