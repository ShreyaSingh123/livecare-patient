import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { DatePipe } from "@angular/common";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ComponentsModule } from "./components/components.module";
import { ModalModule } from "ngx-bootstrap/modal";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxSpinnerModule } from "ngx-spinner";
import { LoadingBarRouterModule } from "@ngx-loading-bar/router";
import { AuthModule } from "./auth/auth.module";
import { ToastrModule } from "ngx-toastr";
import { AuthGuard } from "./shared/guards/auth.guard";
import {
  SocialLoginModule,
  SocialAuthServiceConfig,
} from "angularx-social-login";
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angularx-social-login";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { JwtInterceptor } from "./shared/helpers/jwt.interceptor";
import { DoctorListService } from "./shared/services/doctorList.service";
import { AuthService } from "./shared/services/auth.service";
import { PrescriptionService } from "./shared/services/prescription.service";
import { BookAppointmentComponent } from "./shared/calendar/book-appointment/book-appointment.component";
import { HeaderComponent } from "./shared/layout/header/header.component";
import { SidebarComponent } from "./shared/layout/sidebar/sidebar.component";

@NgModule({
  declarations: [
    AppComponent,
    BookAppointmentComponent,
    HeaderComponent,
    SidebarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    LoadingBarRouterModule,
    AuthModule,
    ComponentsModule,
    SocialLoginModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: "toast-bottom-right",
      preventDuplicates: true,
    }),
  ],
  exports: [],

  providers: [
    DatePipe,
    AuthGuard,
    DoctorListService,
    PrescriptionService,
    AuthService,
    {
      provide: "SocialAuthServiceConfig",
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              "608042274248-5v4cp6vf6ohikmgpsfkutglm1rbn0m5g.apps.googleusercontent.com"
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider("104566368180753"),
          },
        ],
      } as SocialAuthServiceConfig,
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
