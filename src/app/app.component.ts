import { Component } from "@angular/core";
declare var $: any;
import * as AOS from "aos";
import { NgxSpinnerService } from "ngx-spinner";
import { Router, NavigationStart } from "@angular/router";
import { AuthService } from "./shared/services/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "LiveCare24";
  hiddenHeaderAside = false;
  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    public auth: AuthService
  ) {}

  ngOnInit(): void {
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 2000);

    this.hiddenHeaderAside = false;
    this.router.events.subscribe((defaultpage) => {
      if (defaultpage instanceof NavigationStart) {
        if (
          defaultpage.url === "/" ||
          defaultpage.url === "/login" ||
          defaultpage.url === "/reset-password" ||
          defaultpage.url === "**" ||
          defaultpage.url === "/register" ||
          defaultpage.url === "/forgot" ||
          defaultpage.url === "/reset" ||
          defaultpage.url === "/verify" ||
          defaultpage.url === "/user-info"
        ) {
          this.hiddenHeaderAside = false;
        } else {
          this.hiddenHeaderAside = true;
        }
      }
    });
    AOS.init();
    $(".flip").click(function (e) {
      $(".panel , .body-content").toggleClass("open");
    });
  }
}
