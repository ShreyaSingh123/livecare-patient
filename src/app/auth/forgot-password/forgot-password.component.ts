import { Component, OnInit } from "@angular/core";
import { FormGroup, FormArray, FormBuilder, Validators } from "@angular/forms";

import { AuthService } from "src/app/shared/services/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.css"],
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  submitted = false;
  otp: any;

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) {
        control.markAsTouched();
        this.markFormTouched(control);
      } else {
        control.markAsTouched();
      }
    });
  }
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private route: Router
  ) {}

  ngOnInit() {
    this.forgotPasswordForm = this.fb.group({
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ],
      ],
    });
  }
  get email() {
    return this.forgotPasswordForm.get("email");
  }
  onForgotPassword() {
    this.spinner.show();
    this.submitted = true;
    this.markFormTouched(this.forgotPasswordForm);
    if (this.forgotPasswordForm.invalid) {
      this.spinner.hide();
      return;
    }
    this.authService.sharePostedData(this.forgotPasswordForm.value);
    this.authService
      .forgotPassword(this.forgotPasswordForm.value)
      .subscribe((res) => {
        if (res.status) {
          this.spinner.hide();
          this.otp = res.data.otpCode;
          localStorage.setItem("otp", this.otp);

          this.toastr.success(res.message);

          this.route.navigateByUrl("/reset");
          //  alert('your otp is ' + data.data.otpCode)
        } else {
          this.spinner.hide();
          this.toastr.error(res.message);
        }
      });
  }
}
