import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { AuthService } from "src/app/shared/services/auth.service";
import { ConstantValues } from "src/app/shared/enums/constant-values.enum";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import {
  SocialAuthService,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angularx-social-login";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;
  socialloginForm: FormGroup;
  googleLoginDetails: any;
  facebookLoginDetials: any;
  isActive = false;
  show: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private socialAuthService: SocialAuthService
  ) {
    this.show = false;
  }

  ngOnInit() {
    localStorage.clear();
    if (this.auth.currentUser == null) {
      this.router.navigate(["/login"]);
    } else {
      this.router.navigate(["/home"]);
    }
    this.loginForm = this.formBuilder.group({
      email: [
        "shreyasingh190@mailinator.com",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ],
      ],
      password: [
        "Abs@123456",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ],
      ],
      deviceType: new FormControl(ConstantValues.consDeviceType),
      deviceToken: new FormControl(ConstantValues.consDeviceToken),
    });
  }

  get email() {
    return this.loginForm.get("email");
  }

  get password() {
    return this.loginForm.get("password");
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      this.spinner.hide();
      return;
    }

    // display form values on success

    this.auth.login(this.loginForm.value).subscribe((res) => {
      localStorage.setItem("email", res.data.email);
      localStorage.setItem("accessToken", res.data.accessToken);
      if (res.status) {
        this.router.navigateByUrl("/home");
        this.spinner.hide();
      } else {
        if (res.data.isEmailVerified == false) {
          this.spinner.hide();
          this.toastr.error(res.message);
          this.router.navigateByUrl("/verify");
        } else {
          this.spinner.hide();
          this.toastr.error(res.message);
        }
      }
    });
  }

  signInWithGoogle(value: any) {
    this.socialAuthService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((googleLoginDetails) => {
        this.googleLoginDetails = googleLoginDetails;
        this.socialloginForm = this.formBuilder.group({
          firstName: this.googleLoginDetails.firstName,
          lastName: this.googleLoginDetails.lastName,
          loginType: this.googleLoginDetails.provider,
          email: this.googleLoginDetails.email,
          socialId: this.googleLoginDetails.id,
          deviceType: new FormControl(ConstantValues.consDeviceType).value,
          deviceToken: new FormControl(ConstantValues.consDeviceToken).value,
        });

        this.auth.googleLogin(this.socialloginForm.value).subscribe((res) => {
          if (res.status) {
            this.toastr.success(res.message);
            this.router.navigateByUrl("/home");
            this.spinner.hide();
          } else {
            this.toastr.error(res.message);
            this.spinner.hide();
          }
        });
      });
  }

  signInWithFacebook() {
    this.socialAuthService
      .signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((facebookLoginDetials) => {
        this.facebookLoginDetials = facebookLoginDetials;
        this.socialloginForm = this.formBuilder.group({
          firstName: this.facebookLoginDetials.firstName,
          lastName: this.facebookLoginDetials.lastName,
          LoginType: this.facebookLoginDetials.provider,
          email: this.facebookLoginDetials.email,
          socialId: this.facebookLoginDetials.id,
          deviceType: new FormControl(ConstantValues.consDeviceType).value,
          deviceToken: new FormControl(ConstantValues.consDeviceToken).value,
        });
        this.auth.facebookLogin(this.socialloginForm.value).subscribe((res) => {
          if (res.status) {
            this.router.navigateByUrl("/home");
            this.spinner.hide();
          } else {
            this.toastr.error(res.message);
            this.spinner.hide();
            alert("unsuccess");
          }
        });
      });
  }
  signOut(): void {
    this.socialAuthService.signOut();
  }
}
