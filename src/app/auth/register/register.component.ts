import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { AuthService } from "src/app/shared/services/auth.service";
import { ConstantValues } from "src/app/shared/enums/constant-values.enum";
declare let $: any;
import { DatePipe } from "@angular/common";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  dialCodeValue: any;
  defaultDialCodeValue: string = "+91";
  maxDate: Date;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,

    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private changeDetector: ChangeDetectorRef
  ) {
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: [
        "",
        [
          Validators.required,
          Validators.email,

          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ],
      ],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ],
      ],
    });
  }

  get registerFormError() {
    return this.registerForm.controls;
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  onCountryChange(event) {
    this.defaultDialCodeValue = event.dialCode;
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    var data = {
      email: this.registerForm.controls["email"].value,
      password: this.registerForm.controls["password"].value,
      deviceType: new FormControl(ConstantValues.consDeviceType).value,
      deviceToken: new FormControl(ConstantValues.consDeviceToken).value,
    };

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.spinner.hide();
      return;
    }
    this.auth.register(data).subscribe((res) => {
      localStorage.setItem("email", data.email);

      localStorage.setItem("accessToken", res.data.accessToken);
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.router.navigateByUrl("/verify");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }
}
