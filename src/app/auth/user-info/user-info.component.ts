import { DatePipe } from "@angular/common";
import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import {
  ConfigurationOptions,
  NumberResult,
  OutputOptionsEnum,
  ContentOptionsEnum,
  SortOrderEnum,
} from "intl-input-phone";

import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-user-info",
  templateUrl: "./user-info.component.html",
  styleUrls: ["./user-info.component.css"],
})
export class UserInfoComponent implements OnInit {
  registerForm: FormGroup;
  submitted: boolean = false;
  dialCodeValue: any;

  phoneNumber: any;
  maxDate: Date;
  configOption4: ConfigurationOptions;
  OutputValue2: NumberResult = new NumberResult();

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private datePipe: DatePipe,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private changeDetector: ChangeDetectorRef
  ) {
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);

    this.configOption4 = new ConfigurationOptions();
    this.configOption4.SelectorClass = "OptionType3";
    this.configOption4.SortBy = SortOrderEnum.CountryName;
    this.configOption4.OptionTextTypes = [];
    this.configOption4.OptionTextTypes.push(ContentOptionsEnum.Flag);
    this.configOption4.OptionTextTypes.push(ContentOptionsEnum.CountryName);
    this.configOption4.OptionTextTypes.push(
      ContentOptionsEnum.CountryPhoneCode
    );
    this.configOption4.OutputFormat = OutputOptionsEnum.Number;
  }

  ngOnInit() {
    var previousData = localStorage.getItem("accessToken");
    if (previousData == null) {
      this.router.navigateByUrl("login");
    }
    this.registerForm = this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      dateOfBirth: ["", Validators.required],
      genderId: ["", Validators.required],
      phoneNumber: ["", Validators.required],
    });
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    var data = {
      firstName: this.registerForm.controls["firstName"].value,
      lastName: this.registerForm.controls["lastName"].value,
      genderId: parseInt(this.registerForm.controls["genderId"].value),
      dateOfBirth: this.datePipe.transform(
        this.registerForm.controls["dateOfBirth"].value,
        "dd-MM-yyyy"
      ),
      phoneNumber: this.phoneNumber,

      dialCode: this.dialCodeValue.replace(/[^A-Z0-9]/gi, ""),
    };

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.spinner.show();
    this.auth.updatePatientProfile(data).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.router.navigateByUrl("/home");
        localStorage.removeItem("accessToken");
        localStorage.removeItem("email");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }

  onNumberChage2(outputResult) {
    this.OutputValue2 = outputResult;
    this.dialCodeValue = outputResult.CountryModel.CountryPhoneCode;
    this.phoneNumber = outputResult.Number;
  }
}
