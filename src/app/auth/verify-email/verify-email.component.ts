import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/shared/services/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-verify-email",
  templateUrl: "./verify-email.component.html",
  styleUrls: ["./verify-email.component.css"],
})
export class VerifyEmailComponent implements OnInit {
  otp: string;
  email: string;

  data: any;
  resetForm: FormGroup;
  submitted = false;
  otpOnInput: any;
  some: any;
  isDisabled: boolean;

  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    var previousData = localStorage.getItem("email");
    if (previousData == null) {
      this.router.navigateByUrl("login");
    }
    this.email = localStorage.getItem("email");
    this.data = {
      email: this.email,
    };

    this.auth.resendEmailCode(this.data).subscribe((res) => {
      this.otpOnInput = res.data.otpcode;
    });
  }

  onOtpChange(event) {
    //  this.otp = event;
    if (event.length == 4) {
      this.otp = event;
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }

  onSubmit() {
    this.submitted = true;
    this.spinner.show();

    // stop here if form is invalid

    var submittedData = {
      otp: this.otp,
      email: this.email,
    };

    this.auth.onVerifyEmail(submittedData).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.router.navigate(["/user-info"]);
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }
  resendOtp() {
    this.auth.resendEmailCode(this.data).subscribe((res) => {
      this.otpOnInput = res.data.otpcode;
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }

  onCancel() {
    this.router.navigate(["/login"]);
  }
}
