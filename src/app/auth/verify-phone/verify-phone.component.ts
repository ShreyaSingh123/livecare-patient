import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-verify-phone",
  templateUrl: "./verify-phone.component.html",
  styleUrls: ["./verify-phone.component.css"],
})
export class VerifyPhoneComponent implements OnInit {
  data: any;
  resetForm: FormGroup;
  submitted: boolean = false;
  dataForVerifyPhone: any;
  isDisabled: boolean;
  otp: string;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private toastr: ToastrService,
    private route: Router,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.dataForVerifyPhone = JSON.parse(
      localStorage.getItem("dataForVerifiedPhone")
    );

    this.data = {
      dialCode: this.dataForVerifyPhone.dialCode.replace(/[^A-Z0-9]/gi, ""),
      phoneNo: this.dataForVerifyPhone.phoneNo.replace(/[^A-Z0-9]/gi, ""),
    };

    this.auth.ResendPhoneOtp(this.data).subscribe((res) => {});
    this.resetForm = this.formBuilder.group({
      code: ["", Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.resetForm.controls;
  }

  onOtpChange(event) {
    //  this.otp = event;
    if (event.length == 4) {
      this.otp = event;
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }
  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    var data1 = this.resetForm.value;
    // stop here if form is invalid
    if (this.resetForm.invalid) {
      this.spinner.hide();
      return;
    }

    this.auth.VerifyPhoneNumber(data1).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.route.navigateByUrl("profile");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
    // }
  }

  resendOtp() {
    this.auth.ResendPhoneOtp(this.data).subscribe((res) => {
      this.spinner.show();

      if (res.status) {
        this.otp = res.data.otpcode;
        this.spinner.hide();
        this.toastr.success(res.message);
        this.route.navigateByUrl("/verify-phone");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }

  onCancel() {
    this.router.navigate(["/profile"]);
  }

  ngOnDestroy() {
    localStorage.removeItem("dataForVerifiedPhone");
  }
}
