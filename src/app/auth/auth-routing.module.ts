import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { VerifyEmailComponent } from "./verify-email/verify-email.component";
import { VerifyPhoneComponent } from "./verify-phone/verify-phone.component";
import { UserInfoComponent } from "./user-info/user-info.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/login",
    pathMatch: "full",
  },
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "register",
    component: RegisterComponent,
  },
  {
    path: "forgot",
    component: ForgotPasswordComponent,
  },

  {
    path: "reset",
    component: ResetPasswordComponent,
  },

  {
    path: "user-info",
    component: UserInfoComponent,
  },

  {
    path: "change-password",
    component: ChangePasswordComponent,
  },
  {
    path: "verify",
    component: VerifyEmailComponent,
  },
  {
    path: "verify-phone",
    component: VerifyPhoneComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
