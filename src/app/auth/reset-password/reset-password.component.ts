import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/shared/services/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { MustMatch } from "src/app/shared/helpers/must-match.validator";
@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.css"],
})
export class ResetPasswordComponent implements OnInit {
  otp: string;
  resetForm: FormGroup;
  submitted = false;
  forgetEmail: any;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastr: ToastrService,
    private route: Router,
    private spineer: NgxSpinnerService
  ) {}
  ngOnInit() {
    this.authService.forgetEmail.subscribe((res) => {
      this.forgetEmail = res.email;
    });

    this.otp = localStorage.getItem("otp");
    this.resetForm = this.formBuilder.group(
      {
        otp: ["", Validators.required],
        newPassword: [
          "",
          [
            Validators.required,
            Validators.minLength(8),
            Validators.pattern(
              "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
            ),
          ],
        ],
        confirmPassword: ["", Validators.required],
      },
      {
        validator: MustMatch("newPassword", "confirmPassword"),
      }
    );
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.resetForm.controls;
  }

  onSubmit() {
    this.spineer.show();
    this.submitted = true;

    var data = {
      otp: this.resetForm.controls["otp"].value,
      newPassword: this.resetForm.controls["newPassword"].value,
      email: this.forgetEmail,
    };

    // stop here if form is invalid
    if (this.resetForm.invalid) {
      this.spineer.hide();
      return;
    }

    this.authService.onResetPassword(data).subscribe((res) => {
      if (res.status) {
        this.spineer.hide();
        this.toastr.success(res.message);
        this.route.navigateByUrl("/login");
      } else {
        this.spineer.hide();
        this.toastr.error(res.message);
      }
    });
  }
}
