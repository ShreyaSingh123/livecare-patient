import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/shared/services/auth.service";
import { Router } from "@angular/router";
import { MustMatch } from "src/app/shared/helpers/must-match.validator";
import { ToastrService } from "ngx-toastr";
import { NgxSpinner } from "ngx-spinner/lib/ngx-spinner.enum";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.css"],
})
export class ChangePasswordComponent implements OnInit {
  registerForm: FormGroup;
  data: any;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toast: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group(
      {
        oldPassword: ["", Validators.required],
        newPassword: [
          "",
          [
            Validators.required,
            Validators.minLength(8),
            Validators.pattern(
              "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
            ),
          ],
        ],
        confirmPassword: ["", Validators.required],
      },
      {
        validator: MustMatch("newPassword", "confirmPassword"),
      }
    );
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    if (this.registerForm.invalid) {
      this.spinner.hide();
      return;
    }
    var data = {
      oldPassword: this.registerForm.controls["oldPassword"].value,
      newPassword: this.registerForm.controls["newPassword"].value,
    };

    this.authService.onChangePassword(data).subscribe((res) => {
      if (res.status) {
        this.toast.success(res.message);
        this.spinner.hide();
        this.router.navigateByUrl("/home");
      } else {
        this.spinner.hide();
        this.toast.warning(res.message);
      }
    });
  }
}
