import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AuthRoutingModule } from "./auth-routing.module";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { Ng2TelInputModule } from "ng2-tel-input";
import { HttpClientModule } from "@angular/common/http";
import { MaterialModule } from "../shared/main-material/material/material.module";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";

import { ChangePasswordComponent } from "./change-password/change-password.component";
import { VerifyEmailComponent } from "./verify-email/verify-email.component";

import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { VerifyPhoneComponent } from "./verify-phone/verify-phone.component";
import { UserInfoComponent } from "./user-info/user-info.component";
import { SharedModule } from "../shared/shared.module";
import { IntlInputPhoneModule } from "intl-input-phone";
import { NgOtpInputModule } from "ng-otp-input";
import { MatPasswordStrengthModule } from "@angular-material-extensions/password-strength";

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    VerifyEmailComponent,
    VerifyPhoneComponent,
    UserInfoComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2TelInputModule,
    HttpClientModule,
    MaterialModule,
    BsDatepickerModule.forRoot(),
    SharedModule,
    IntlInputPhoneModule,
    NgOtpInputModule,
    MatPasswordStrengthModule.forRoot(),
  ],
  exports: [],
  providers: [],
})
export class AuthModule {}
