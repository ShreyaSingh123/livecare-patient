import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "src/app/shared/services/auth.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-blog",
  templateUrl: "./blog.component.html",
  styleUrls: ["./blog.component.css"],
})
export class BlogComponent implements OnInit {
  List: Array<any>;
  rootUrl: any;

  totalCount: any;
  currentPage: number = 1;
  page: number = 1;
  pageSize: number = environment.defaultPageSize;
  @ViewChild("backToTop")
  backToTop: ElementRef;

  constructor(private auth: AuthService, private spinner: NgxSpinnerService) {}

  ngOnInit() {
    this.rootUrl = environment.baseImgUrl;
    this.getBlogsList();
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.getBlogsList();
  }

  getBlogsList() {
    this.spinner.show();
    this.rootUrl = environment.baseImgUrl;
    this.auth.getBlogList(this.page, this.pageSize).subscribe((res) => {
      this.List = res.data.dataList;
      this.totalCount = res.data.totalCount;
      this.spinner.hide();
      this.backToTop.nativeElement.scrollIntoView({ behavior: "smooth" });
    });
  }
}
