import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { ActivatedRoute, Params } from "@angular/router";
import { environment } from "src/environments/environment";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-blog-details",
  templateUrl: "./blog-details.component.html",
  styleUrls: ["./blog-details.component.css"],
})
export class BlogDetailsComponent implements OnInit {
  blogId: any;
  rootUrl: any;
  blogDetails: any;
  constructor(
    private auth: AuthService,

    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.blogId = this.route.snapshot.paramMap.get("id");
    this.getBlogDetails(this.blogId);
    this.rootUrl = environment.baseImgUrl;
  }

  getBlogDetails(id: any) {
    this.spinner.show();
    this.auth.getBlogDetails(id).subscribe((res) => {
      this.blogDetails = res.data;
      this.spinner.hide();
    });
  }
}
