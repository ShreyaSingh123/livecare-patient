import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ComponentsRoutingModule } from "./components-routing.module";
import { MaterialModule } from "../shared/main-material/material/material.module";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";

import { BlogComponent } from "./main-blog/blog/blog.component";
import { MyMedicalRecordsComponent } from "./medical-records/my-medical-records/my-medical-records.component";
import { PrescriptionComponent } from "./my-prescription/prescription/prescription.component";
import { MyConsultComponent } from "./consult/my-consult/my-consult.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { SettingsComponent } from "./setting/settings/settings.component";
import { AboutUsComponent } from "./about/about-us/about-us.component";
import { BasicComponent } from "./patient-info/basic/basic.component";
import { MyProfileComponent } from "./my-profile/my-profile.component";
import { AdditionalInfoComponent } from "./patient-info/additional-info/additional-info.component";
import { Ng2TelInputModule } from "ng2-tel-input";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { PastMedicalHistoryComponent } from "./medical-records/past-medical-history/past-medical-history.component";
import { SurgicalHistoryComponent } from "./medical-records/surgical-history/surgical-history.component";
import { FamilyHistoryComponent } from "./medical-records/family-history/family-history.component";
import { AllergyHistoryComponent } from "./medical-records/allergy-history/allergy-history.component";
import { PastMedicalListComponent } from "./medical-records/past-medical-list/past-medical-list.component";
import { FamilyHistoryListComponent } from "./medical-records/family-history-list/family-history-list.component";
import { SurgicalHistoryListComponent } from "./medical-records/surgical-history-list/surgical-history-list.component";
import { AllergyHistoryListComponent } from "./medical-records/allergy-history-list/allergy-history-list.component";
import { AttachmentReportListComponent } from "./medical-records/attachment-report-list/attachment-report-list.component";
import { AttachmenHistoryComponent } from "./medical-records/attachmen-history/attachmen-history.component";
import { SafePipe } from "../shared/pipes/safe.pipe";
import { ContactUsComponent } from "./setting/contact-us/contact-us.component";
import { PrivacyPolicyComponent } from "./setting/privacy-policy/privacy-policy.component";
import { TermsConditionComponent } from "./setting/terms-condition/terms-condition.component";
import { BlogDetailsComponent } from "./main-blog/blog-details/blog-details.component";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { PrescriptionDetailsComponent } from "./my-prescription/prescription-details/prescription-details.component";
import { NgCircleProgressModule } from "ng-circle-progress";
// import { RatingModule } from "ng-starrating";
import { TopDoctorAllListComponent } from "./home/top-doctor-all-list/top-doctor-all-list.component";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { AllDoctorListComponent } from "./home/all-doctor-list/all-doctor-list.component";
import { SpecialitiesComponent } from "./home/specialities/specialities.component";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { MainHomeComponent } from "./home/main-home/main-home.component";

import { SpecialitiesAllListComponent } from "./home/specialities-all-list/specialities-all-list.component";
import { SpecialitiesDetailsComponent } from "./home/specialities-details/specialities-details.component";
import { DoctorInfoComponent } from "./home/doctor-info/doctor-info.component";
import { DoctorBookingComponent } from "./home/booking/doctor-booking/doctor-booking.component";

import { BookingRequestComponent } from "./home/booking/booking-request/booking-request.component";
import { UserReviewInfoComponent } from "./review/user-review-info/user-review-info.component";
import { ConfirmedConsultComponent } from "./consult/confirmed-consult/confirmed-consult.component";
import { PendingConsultsComponent } from "./consult/pending-consults/pending-consults.component";
import { PastConsultsComponent } from "./consult/past-consults/past-consults.component";
import { UserRatingComponent } from "./review/user-rating/user-rating.component";
import { SortByPipe } from "../shared/pipes/sort.pipe";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { RatingModule } from "ngx-bootstrap/rating";
import { CountdownModule } from "ngx-countdown";
import { SharedModule } from "../shared/shared.module";
import { IntlInputPhoneModule } from "intl-input-phone";
import { CallingDoctorComponent } from "./home/calling-doctor/calling-doctor.component";
import { PublisherComponent } from "./home/calling-doctor/publisher/publisher.component";
import { SubscriberComponent } from "./home/calling-doctor/subscriber/subscriber.component";

@NgModule({
  declarations: [
    BlogComponent,
    SettingsComponent,
    MyConsultComponent,
    MyMedicalRecordsComponent,
    PrescriptionComponent,
    NotificationsComponent,
    AboutUsComponent,
    BasicComponent,
    MyProfileComponent,
    AdditionalInfoComponent,
    PastMedicalHistoryComponent,
    SurgicalHistoryComponent,
    FamilyHistoryComponent,
    AllergyHistoryComponent,
    PastMedicalListComponent,
    FamilyHistoryListComponent,
    SurgicalHistoryListComponent,
    AllergyHistoryListComponent,
    AttachmentReportListComponent,
    AttachmenHistoryComponent,
    SafePipe,
    SortByPipe,

    ContactUsComponent,
    PrivacyPolicyComponent,
    TermsConditionComponent,
    BlogDetailsComponent,
    PrescriptionDetailsComponent,
    TopDoctorAllListComponent,
    AllDoctorListComponent,
    SpecialitiesComponent,
    MainHomeComponent,
    SpecialitiesAllListComponent,
    SpecialitiesDetailsComponent,
    DoctorInfoComponent,
    DoctorBookingComponent,
    BookingRequestComponent,
    UserReviewInfoComponent,
    ConfirmedConsultComponent,
    PendingConsultsComponent,
    PastConsultsComponent,
    UserRatingComponent,
    CallingDoctorComponent,
    PublisherComponent,
    SubscriberComponent,
  ],

  imports: [
    CommonModule,
    ComponentsRoutingModule,
    MaterialModule,
    CountdownModule,
    SharedModule,
    FormsModule,
    ModalModule.forRoot(),
    NgxDatatableModule,
    ReactiveFormsModule,
    Ng2TelInputModule,
    Ng2SearchPipeModule,
    RatingModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    IntlInputPhoneModule,
    BsDatepickerModule.forRoot(),
    NgCircleProgressModule.forRoot({
      radius: 60,
      space: -10,
      outerStrokeGradient: true,
      outerStrokeWidth: 10,
      outerStrokeColor: "#4882c2",
      outerStrokeGradientStopColor: "#17a2b8",
      innerStrokeColor: "#e7e8ea",
      innerStrokeWidth: 10,
      animationDuration: 1000,
      showBackground: false,
    }),
    // RatingModule,
  ],
  exports: [BasicComponent, SortByPipe],
})
export class ComponentsModule {}
