import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { UserReviewRatingService } from "src/app/shared/services/userReviewRating.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-user-rating",
  templateUrl: "./user-rating.component.html",
  styleUrls: ["./user-rating.component.css"],
})
export class UserRatingComponent implements OnInit {
  profilePic: any;
  rootUrl: any;
  appointmentId: any;
  comments: any;
  item: any;
  ratingForm: FormGroup;
  submitted = false;
  constructor(
    private userReviewRating: UserReviewRatingService,
    private toastr: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.ratingForm = this.formBuilder.group({
      comments: ["", Validators.required, ,],
      rating: ["", Validators.required],
    });

    this.rootUrl = environment.baseImgUrl;
    this.item = JSON.parse(localStorage.getItem("dataForRating"));
  }

  onSubmit() {
    this.submitted = true;

    var data = {
      rating: this.ratingForm.controls["rating"].value,
      comments: this.ratingForm.controls["comments"].value,
      appointmentId: this.item.appointmentId,
    };
    this.spinner.show();

    this.userReviewRating.updateUserRating(data).subscribe((res) => {
      if (res.status) {
        this.toastr.success(res.message);
        this.router.navigate(["/home"]);
        this.spinner.hide();
      } else {
        this.toastr.warning(res.message);
        this.spinner.hide();
      }
    });
  }

  ngOnDestroy(): void {
    localStorage.removeItem("dataForRating");
  }
}
