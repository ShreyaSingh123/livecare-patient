import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import * as moment from "moment";

import { UserReviewRatingService } from "src/app/shared/services/userReviewRating.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-user-review-info",
  templateUrl: "./user-review-info.component.html",
  styleUrls: ["./user-review-info.component.css"],
})
export class UserReviewInfoComponent implements OnInit {
  doctorId: any;
  reviewList: any;
  rootUrl: any;
  showUserReview: boolean = false;
  constructor(
    private route: ActivatedRoute,

    private userReview: UserReviewRatingService
  ) {}

  ngOnInit(): void {
    this.rootUrl = environment.baseImgUrl;
    this.doctorId = this.route.snapshot.paramMap.get("id");
    this.userReview.getUserReview(this.doctorId).subscribe((res) => {
      this.reviewList = res.data.map(function (user) {
        return {
          date: moment(user.date, "DD-MM-YYYY HH:mm:ss").format("DD-MM-YYYY"),
          name: user.name,
          patientId: user.patientId,
          profilePic: user.profilePic,
          reviews: user.reviews,
        };
      });

      if (res.data.length == 0) {
        this.showUserReview = true;
      } else {
        this.showUserReview = false;
      }
    });
  }
}
