import { Component, OnInit, ɵConsole } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "src/app/shared/services/auth.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.component.html",
  styleUrls: ["./my-profile.component.css"],
})
export class MyProfileComponent implements OnInit {
  _fileData: File = null;
  profilePic: string;
  viewData: any;
  url: any;

  constructor(private auth: AuthService, private spinner: NgxSpinnerService) {}
  ngOnInit() {
    this.spinner.show();
    this.auth.getProfessionalInfo().subscribe((data) => {
      if (data.data.profilePic != null) {
        this.url = data.data.profilePic;
        this.viewData = data.data;

        this.getProfilePic();
      } else {
        if (data.data.genderId) {
        }
      }
      this.spinner.hide();
    });
  }
  changeProfilePic(fileInput: any) {
    this._fileData = <File>fileInput.target.files[0];
    this.onUpdateProfilePic();
  }
  onUpdateProfilePic() {
    const formData = new FormData();
    formData.append("imgFile", this._fileData, this._fileData.name);
    this.auth.updateProfilePic(formData).subscribe((res) => {
      if (res.status) {
        localStorage.setItem("profilePic", res.data.profilepicurl);

        this.profilePic =
          environment.baseImgUrl + localStorage.getItem("profilePic");
      }
    });
  }

  showProfilePic(src: any) {
    this.profilePic = src;
  }
  getProfilePic() {
    this.profilePic = environment.baseImgUrl + this.url;
  }
}
