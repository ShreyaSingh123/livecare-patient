import { Component, OnInit, TemplateRef } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "src/app/shared/services/auth.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-additional-info",
  templateUrl: "./additional-info.component.html",
  styleUrls: ["./additional-info.component.css"],
})
export class AdditionalInfoComponent implements OnInit {
  public modalRef: BsModalRef;
  additionalForm: FormGroup;
  disable: boolean = false;
  show = false;
  buttonName = "Edit";
  hide: any;
  submitted = false;
  aditionalInfo: any;
  height(n: number, startFrom: number): number[] {
    return [...Array(n).keys()].map((i) => i + startFrom);
  }
  arrayHeightInFeet = [
    "4'0\"",
    "4'1\"",
    "4'2\"",
    "4'3\"",
    "4'4\"",
    "4'5\"",
    "4'6\"",
    "4'7\"",
    "4'8\"",
    "4'9\"",
    "4'10\"",
    "4'11\"",
    "5'0\"",
    "5'1\"",
    "5'2\"",
    "5'3\"",
    "5'4\"",
    "5'5\"",
    "5'6\"",
    "5'7\"",
    "5'8\"",
    "5'9\"",
    "5'10\"",
    "5'11\"",
    "6'0\"",
    "6'1\"",
    "6'2\"",
    "6'3\"",
    "6'4\"",
    "6'5\"",
    "6'6\"",
    "6'7\"",
    "6'8\"",
    "6'9\"",
    "6'10\"",
    "6'11\"",
    "7'0\"",
  ];
  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private spinner: NgxSpinnerService
  ) {}
  ngOnInit() {
    // this.auth.getPatientDashboardInfo().subscribe((res => {
    // }))
    this.getData();
    (this.additionalForm = new FormGroup({
      height: new FormControl(""),
      weight: new FormControl(""),
      bloodGroup: new FormControl(""),
      isVegetarian: new FormControl(""),
      useAlcohol: new FormControl(""),
      useSmoke: new FormControl(""),
      useDrug: new FormControl(""),
    })),
      this.additionalForm.disable();
  }

  getData() {
    this.auth.PatientAdditionalInfo().subscribe((res) => {
      this.aditionalInfo = res.data;
      this.additionalForm.controls["height"].setValue(
        this.aditionalInfo.height + " cms"
      );
      this.additionalForm.controls["weight"].setValue(
        this.aditionalInfo.weight + " lbs"
      );
      this.additionalForm.controls["bloodGroup"].setValue(
        this.aditionalInfo.bloodGroup
      );
      this.additionalForm.controls["isVegetarian"].setValue(
        this.aditionalInfo.isVegetarian
      );
      this.additionalForm.controls["useAlcohol"].setValue(
        this.aditionalInfo.useAlcohol
      );
      this.additionalForm.controls["useSmoke"].setValue(
        this.aditionalInfo.useSmoke
      );
      this.additionalForm.controls["useDrug"].setValue(
        this.aditionalInfo.useDrug
      );
    });
  }

  submit() {
    this.submitted = true;
    this.additionalForm.enable();
    this.show = !this.show;
    if (this.show) {
      (this.buttonName = "Save"), this.additionalForm.enable();
    } else {
      // if (this.additionalForm.invalid) {
      //   Swal.fire("Please Complete the entire information");
      //   return;
      // }
      var data = {
        height: parseFloat(this.additionalForm.controls["height"].value),
        weight: parseFloat(this.additionalForm.controls["weight"].value),
        bloodGroup: this.additionalForm.controls["bloodGroup"].value,
        isVegetarian: JSON.parse(
          this.additionalForm.controls["isVegetarian"].value
        ),
        useSmoke: JSON.parse(this.additionalForm.controls["useSmoke"].value),
        useAlcohol: JSON.parse(
          this.additionalForm.controls["useAlcohol"].value
        ),
        useDrug: JSON.parse(this.additionalForm.controls["useDrug"].value),
      };

      for (var key in data) {
        if (data[key] === "") {
          Swal.fire("Please Complete the entire information");
          return;
        }
      }
      this.spinner.show();
      this.auth.patientHealthInfo(data).subscribe((res) => {
        this.spinner.hide();
        this.toastr.success(res.message);
      });
      (this.buttonName = "Edit"), this.additionalForm.disable();
    }
  }

  openHeightModal(heighttemplate: TemplateRef<any>) {
    this.modalRef = this.modalService.show(heighttemplate);
    this.modalRef.setClass("modal-lg");
  }
  openWeightModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass("modal-lg");
  }

  getHeightValueinCms(value: any) {
    this.additionalForm.controls["height"].setValue(value + " cms");
    this.modalRef.hide();
  }
  getHeightValueinfeet(value: any) {
    var str = value.replace("'", ".");
    var feetInCm = str.replace('"', "");
    var cm = 0.032808;
    this.additionalForm.controls["height"].setValue(
      (feetInCm / cm).toFixed(2) + " cms"
    );
    this.modalRef.hide();
  }
  getWeightinkg(value: any) {
    var kg = 0.45359237;
    var lbsValue = value / kg;
    this.additionalForm.controls["weight"].setValue(
      lbsValue.toFixed(2) + " lbs"
    );
    this.modalRef.hide();
  }

  getWeightinlbs(value: any) {
    this.additionalForm.controls["weight"].setValue(value + " lbs");
    this.modalRef.hide();
  }
}
