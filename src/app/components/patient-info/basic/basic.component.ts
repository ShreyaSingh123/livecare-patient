import { Component, Input, OnInit, SimpleChange } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import {
  ConfigurationOptions,
  NumberResult,
  OutputOptionsEnum,
  ContentOptionsEnum,
  SortOrderEnum,
} from "intl-input-phone";
import { DatePipe } from "@angular/common";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-basic",
  templateUrl: "./basic.component.html",
  styleUrls: ["./basic.component.css"],
})
export class BasicComponent implements OnInit {
  basicForm: FormGroup;
  countryId: any;
  stateId: any;
  dialCode: any;
  countriesList: any;
  genderList: any;
  genderId: any;
  statesList: any;
  basicInfo: any;
  disable: boolean = false;
  show = false;
  buttonName = "Edit";
  hide: any;
  dateOfBirth: any;
  dialCodeValue: any;
  dateString: any;
  date: any;
  FormattedDate: any;
  phoneNumber: any;
  todayDate: any = new Date();
  @Input() viewCode: string;
  isEmailVerified: any;
  submitted = false;
  emailByApi: any;
  isPhoneNoVerified: any;
  patientPhoneInfo: Array<any> = [];
  configOption4: ConfigurationOptions;
  OutputValue2: NumberResult = new NumberResult();
  phoneNumberForOtp: any;
  setphoneNumber: any;
  maxDate: Date;

  constructor(
    private auth: AuthService,
    private changeDetector: ChangeDetectorRef,
    private tostr: ToastrService,
    private router: Router,
    private datePipe: DatePipe,
    private spinner: NgxSpinnerService
  ) {
    this.configOption4 = new ConfigurationOptions();
    this.configOption4.SelectorClass = "OptionType3";
    this.configOption4.SortBy = SortOrderEnum.CountryName;
    this.configOption4.OptionTextTypes = [];
    this.configOption4.OptionTextTypes.push(ContentOptionsEnum.Flag);
    this.configOption4.OptionTextTypes.push(ContentOptionsEnum.CountryName);
    this.configOption4.OptionTextTypes.push(
      ContentOptionsEnum.CountryPhoneCode
    );
    this.configOption4.OutputFormat = OutputOptionsEnum.Number;

    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
  }

  ngOnInit() {
    this.patientPhoneInfo = [];
    this.dialCode;
    this.getBasicData();
    this.GetCountries();
    (this.basicForm = new FormGroup({
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      dateOfBirth: new FormControl(""),
      genderId: new FormControl(""),
      countryId: new FormControl(""),
      stateId: new FormControl(""),
      city: new FormControl("", Validators.required),
      currentAddress: new FormControl("", Validators.required),
      email: new FormControl("", [Validators.required, Validators.email]),
      phoneNumber: new FormControl("", Validators.required),
    })),
      this.basicForm.disable();
  }

  getData() {
    this.spinner.show();
    this.basicForm.controls["firstName"].setValue(this.basicInfo.firstName);
    this.basicForm.controls["lastName"].setValue(this.basicInfo.lastName);
    this.basicForm.controls["dateOfBirth"].setValue(this.basicInfo.dateOfBirth);
    this.basicForm.controls["genderId"].setValue(this.basicInfo.genderId);
    this.basicForm.controls["countryId"].setValue(this.basicInfo.countryId);
    this.basicForm.controls["stateId"].setValue(this.basicInfo.stateId);
    this.basicForm.controls["city"].setValue(this.basicInfo.city);
    this.basicForm.controls["currentAddress"].setValue(
      this.basicInfo.currentAddress
    );
    this.spinner.hide();
    // this.basicForm.controls["email"].setValue(this.basicInfo.email);
  }

  GetCountries() {
    this.auth.getCountries().subscribe((res) => {
      if (res.status) {
        this.countriesList = res.data.list;
      }
    });
  }

  onChangeCountry(id: any) {
    this.auth.getStatesById(id).subscribe((res) => {
      this.statesList = res.data.list;
    });
  }

  onSubmit() {
    this.submitted = true;
    this.basicForm.enable();
    this.show = !this.show;
    if (this.show) {
      (this.buttonName = "Save"), this.basicForm.enable();
    } else {
      var data = {
        firstName: this.basicForm.controls["firstName"].value,
        lastName: this.basicForm.controls["lastName"].value,
        genderId: parseInt(this.basicForm.controls["genderId"].value),
        dateOfBirth: this.datePipe.transform(
          this.basicForm.controls["dateOfBirth"].value,
          "dd-MM-yyyy"
        ),

        phoneNumber: this.phoneNumber,

        dialCode: this.dialCodeValue.replace(/[^A-Z0-9]/gi, ""),
        countryId: parseInt(this.basicForm.controls["countryId"].value),
        stateId: parseInt(this.basicForm.controls["stateId"].value),
        city: this.basicForm.controls["city"].value,
        currentAddress: this.basicForm.controls["currentAddress"].value,
      };

      for (var key in data) {
        if (data[key] === "") {
          Swal.fire("Please Complete the entire information");
          return;
        }
      }
      this.spinner.show();
      this.auth.updatePatientBasicInfo(data).subscribe((res) => {
        this.spinner.hide();
        this.tostr.success(res.message);
      });
      (this.buttonName = "Edit"), this.basicForm.disable();
    }
  }
  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  get f() {
    return this.basicForm.controls;
  }

  verifyEmail() {
    if (this.isEmailVerified) {
    } else {
      this.router.navigate(["/verifyEmail"]);
    }
  }

  onNumberChage2(outputResult) {
    this.OutputValue2 = outputResult;
    this.dialCodeValue = outputResult.CountryModel.CountryPhoneCode;
    this.phoneNumber = outputResult.Number;
  }

  verifiedPhone() {
    if (this.isPhoneNoVerified) {
    } else {
      var dataForVerifiedPhone = {
        dialCode: this.dialCodeValue,
        phoneNo: this.phoneNumber,
      };
      localStorage.setItem(
        "dataForVerifiedPhone",
        JSON.stringify(dataForVerifiedPhone)
      );

      this.router.navigate(["/verify-phone"]);
    }
  }

  getBasicData() {
    this.auth.getProfessionalInfo().subscribe((res) => {
      this.basicInfo = res.data;
      this.phoneNumber = res.data.phoneNumber;
      if (res.data.dialCode.length === 4) {
        this.dialCodeValue =
          res.data.dialCode.slice(0, 1) +
          " " +
          "(" +
          res.data.dialCode.slice(1) +
          ")";
      } else {
        this.dialCodeValue = res.data.dialCode;
      }

      this.setphoneNumber =
        "+" + this.dialCodeValue + " " + res.data.phoneNumber;
      this.onChangeCountry(res.data.countryId);
      this.isEmailVerified = res.data.isEmailVerified;
      this.isPhoneNoVerified = res.data.isPhoneNoVerified;

      this.emailByApi = res.data.email;
      localStorage.setItem("email", this.emailByApi);
      this.phoneNumberForOtp = res.data.phoneNumber;
      localStorage.setItem("phoneNumber", this.phoneNumberForOtp);
      this.getData();
    });
  }
}
