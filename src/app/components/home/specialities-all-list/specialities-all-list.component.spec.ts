import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialitiesAllListComponent } from './specialities-all-list.component';

describe('SpecialitiesAllListComponent', () => {
  let component: SpecialitiesAllListComponent;
  let fixture: ComponentFixture<SpecialitiesAllListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialitiesAllListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialitiesAllListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
