import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-specialities-all-list",
  templateUrl: "./specialities-all-list.component.html",
  styleUrls: ["./specialities-all-list.component.css"],
})
export class SpecialitiesAllListComponent implements OnInit {
  specialityList: any;
  rootUrl: any;
  specialitiesPlaceholderImg = environment.specilatiesPlaceholderImg;

  constructor(
    private doctorList: DoctorListService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.rootUrl = environment.baseImgUrl;
    this.spinner.show();
    this.doctorList.getSpeciality().subscribe((res) => {
      this.specialityList = res.data.list;
      this.spinner.hide();
    });
  }

  save(data) {
    this.router.navigate(["/specialitiesDetails", data.id]);
  }
}
