import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";

@Component({
  selector: "app-booking-request",
  templateUrl: "./booking-request.component.html",
  styleUrls: ["./booking-request.component.css"],
})
export class BookingRequestComponent implements OnInit {
  DoctorId: any;
  rootUrl: any;
  list: any;
  timeSlotId: any;
  bookingData: any;

  constructor(
    private route: ActivatedRoute,
    private doctorService: DoctorListService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.bookingData = JSON.parse(localStorage.getItem("bookingData"));

    this.DoctorId = this.route.snapshot.paramMap.get("id");

    this.getDoctorInfo(this.DoctorId);
    this.rootUrl = environment.baseImgUrl;
  }
  getDoctorInfo(id: any) {
    this.spinner.show();
    this.doctorService.getDoctorInfo(id).subscribe((res) => {
      this.list = res.data;
      this.spinner.hide();
    });
  }

  requestAppointment() {
    var data = {
      timeSlotId: this.bookingData.timeSlotId,
      doctorId: this.DoctorId,
    };

    Swal.fire({
      title: "Appointment Request",
      html: "Are you sure you want to request for appointment",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.doctorService.bookAppointment(data).subscribe((res) => {
          this.spinner.hide();

          if (res.status == true) {
            this.toastr.success(res.message);
            this.router.navigateByUrl("/home");
          } else {
            this.toastr.warning(res.message);
          }
        });
      }
    });
  }

  ngOnDestroy() {
    localStorage.removeItem("bookingData");
  }
}
