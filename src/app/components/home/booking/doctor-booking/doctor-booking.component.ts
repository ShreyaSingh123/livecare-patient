import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { environment } from "src/environments/environment";
import { DatePipe } from "@angular/common";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-doctor-booking",
  templateUrl: "./doctor-booking.component.html",
  styleUrls: ["./doctor-booking.component.css"],
})
export class DoctorBookingComponent implements OnInit {
  bsInlineValue = new Date();
  DoctorId: any;
  nextMonth: Date;
  previousDate: Date;
  name: any;
  selectedDateValue: any;
  doctorSpeciality;
  profilePic: any;
  rootUrl: any;
  date: any;
  fromDates;
  availableDates: [];
  datesEnable: Array<any> = [];
  disabledPreviousTime: any;
  timeSlotsForPatient;
  selectedDate: Date;
  showTimeSlots: boolean;
  availableSlots = [];
  availableTimeSlots: Array<any> = [];
  fromDatesfortimeslots: Array<any> = [];
  shownOnCalendarTimeSlots: Array<any> = [];
  requiredTimeSlots: Array<any> = [];
  today = moment().format("MM/DD/YYYY");
  timeSlotId: Array<any> = [];
  dataArray: Array<any> = [];
  newData: [];
  doctorInfo: any;

  Dates = [];
  timeSlots: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    private doctorService: DoctorListService,
    private dateFormat: DatePipe,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.rootUrl = environment.baseImgUrl;
    this.previousDate = new Date();
    this.nextMonth = new Date();
    this.nextMonth.setMonth(this.nextMonth.getMonth() + 2);
  }

  ngOnInit(): void {
    this.DoctorId = this.route.snapshot.paramMap.get("id");
    this.spinner.show();
    this.doctorService.getDoctorInfo(this.DoctorId).subscribe((res) => {
      this.doctorInfo = res.data;
      this.spinner.hide();
    });
    this.date = this.dateFormat.transform(this.bsInlineValue, "dd-MM-yyyy");
    this.getAvailableDates();
  }

  getAvailableDates() {
    this.spinner.show();
    this.doctorService
      .getAvailableDatesForPatient(this.DoctorId, this.date)
      .subscribe((res) => {
        this.fromDates = res.data;
        this.spinner.hide();
        this.newData = this.fromDates.map(
          (x) =>
            new Date(
              moment
                .utc(x.fromTime, "DD-MM-YYYY HH:mm:ss")
                .local()
                .format("MM-DD-YYYY HH:mm:ss")
            )
        );

        // this.availableDates = this.fromDates.map((x) => x.fromTime);
        // var i;
        // for (i = 0; i < this.availableDates.length; i++) {
        //   this.fromDatesfortimeslots = this.availableDates[i];
        //   var momentVariable = moment.utc(
        //     this.availableDates[i],
        //     "DD-MM-YYYY HH:mm:ss "
        //   );
        //   var d = momentVariable.local().format("MM/DD/YYYY HH:mm:ss");

        //   this.datesEnable.push(new Date(d));

        //   this.spinner.hide();
        // }
      });
  }

  onSelectDate(value) {
    this.selectedDateValue = moment(value).format("MM-DD-YYYY");

    this.disabledPreviousTime = moment().format("MM-DD-YYYY HH:mm:ss");

    // this.availableTimeSlots = [];
    this.timeSlots = [];
    this.timeSlotsForPatient = this.dateFormat.transform(value, "dd-MM-yyyy");
    //this.timeSlotsForPatient = moment(value).format("DD-MM-YYYYY HH:mm:ss");
    this.spinner.show();
    this.doctorService
      .getTimeSlotsForPatient(this.DoctorId, this.timeSlotsForPatient)
      .subscribe((res) => {
        this.dataArray = res.data;
        this.spinner.hide();

        // this.availableSlots = this.dataArray.map((x) => x.slotFrom);
        // var j;
        // for (j = 0; j < this.availableSlots.length; j++) {
        //   var momentVariable = moment.utc(
        //     this.availableSlots[j],
        //     "DD-MM-YYYY HH:mm:ss"
        //   );
        //   var gh = moment(momentVariable, "DD-MM-YYYY HH:mm:ss")
        //     .local()
        //     .format("MM-DD-YYYY HH:mm:ss");

        //   this.availableTimeSlots.push({ localVal: gh });
        // }
        // this.dataArray.map((obj, index) => {
        //   var internalization = {
        //     localDateValue: Object.keys(this.availableTimeSlots[index])[0],
        //     localValue: this.availableTimeSlots[index][
        //       Object.keys(this.availableTimeSlots[index])[0]
        //     ],
        //   };
        //   return Object.assign(obj, internalization);
        // });

        this.timeSlots = this.dataArray.map(function (user) {
          return {
            isSlotAvailable: user.isSlotAvailable,
            date: user.date,
            slotFrom: user.slotFrom,
            slotTo: user.slotTo,
            timeSlotId: user.timeSlotId,
            localValue: moment
              .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
          };
        });

        this.spinner.show();
        this.requiredTimeSlots = this.timeSlots.filter((s) =>
          s.localValue.includes(this.selectedDateValue)
        );
        this.spinner.hide();
      });
  }

  sendDoctorId(slots) {
    localStorage.setItem("bookingData", JSON.stringify(slots));
    this.spinner.show();

    this.router.navigate(["/booking-request", this.DoctorId]);
  }
}
