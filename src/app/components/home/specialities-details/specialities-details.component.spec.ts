import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialitiesDetailsComponent } from './specialities-details.component';

describe('SpecialitiesDetailsComponent', () => {
  let component: SpecialitiesDetailsComponent;
  let fixture: ComponentFixture<SpecialitiesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialitiesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialitiesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
