import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-specialities-details",
  templateUrl: "./specialities-details.component.html",
  styleUrls: ["./specialities-details.component.css"],
})
export class SpecialitiesDetailsComponent implements OnInit {
  specialityId: any;
  rootUrl: any;
  list: any;
  pageSize: any = 10;
  pageNumber: any = 1;
  term: any;

  constructor(
    private doctorList: DoctorListService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.specialityId = this.route.snapshot.paramMap.get("id");
    this.rootUrl = environment.baseImgUrl;
    this.spinner.show();
    this.doctorList
      .getSpecialityDetails(this.specialityId, this.pageSize, this.pageNumber)
      .subscribe((res) => {
        this.list = res.data.dataList;
        this.spinner.hide();
      });
  }

  save(data) {
    this.router.navigate(["/booking-appointment", data.id]);
  }

  getDoctorInfo(data) {
    this.router.navigate(["/doctor-info", data.id]);
  }
}
