import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-doctor-info",
  templateUrl: "./doctor-info.component.html",
  styleUrls: ["./doctor-info.component.css"],
})
export class DoctorInfoComponent implements OnInit {
  DoctorId: any;
  doctorInfo: any;
  rootUrl: any;
  rating: number;

  constructor(
    private doctorService: DoctorListService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.DoctorId = this.route.snapshot.paramMap.get("id");

    this.getDoctorInfo(this.DoctorId);
    this.rootUrl = environment.baseImgUrl;
  }

  getDoctorInfo(id: any) {
    this.doctorService.getDoctorInfo(id).subscribe((res) => {
      this.doctorInfo = res.data;

      this.rating = this.doctorInfo.rating;
    });
  }

  checkReview() {
    this.router.navigate(["/review", this.DoctorId]);
  }
  bookAppointment() {
    this.router.navigate(["/booking-appointment", this.DoctorId]);
  }
}
