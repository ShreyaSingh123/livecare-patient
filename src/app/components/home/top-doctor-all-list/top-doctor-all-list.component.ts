import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-top-doctor-all-list",
  templateUrl: "./top-doctor-all-list.component.html",
  styleUrls: ["./top-doctor-all-list.component.css"],
})
export class TopDoctorAllListComponent implements OnInit {
  pageSize: number = 10;
  list: any[];
  rootUrl: any;
  currentPage = 1;
  maleImg: any;
  noDoctor: boolean = false;
  doctorSpecialist: any;
  totalCount: any;
  page: number = 1;
  @ViewChild("backToTop")
  backToTop: ElementRef;
  constructor(
    private doctorList: DoctorListService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}
  ngOnInit(): void {
    this.maleImg = environment.maleDmpImg;
    this.rootUrl = environment.baseImgUrl;
    this.getTopDoctorList();
  }
  pageChanged(event: any): void {
    this.page = event.page;
    this.getTopDoctorList();
    this.backToTop.nativeElement.scrollIntoView({ behavior: "smooth" });
  }

  getTopDoctorList() {
    this.spinner.show();
    this.doctorList
      .getDoctorTopList(this.page, this.pageSize)
      .subscribe((res) => {
        this.list = res.data.dataList;

        this.totalCount = res.data.totalCount;
        this.doctorSpecialist = res.data.dataList;
        if (res.data.dataList.length == 0) {
          this.noDoctor = true;
          this.spinner.hide();
        } else {
          this.noDoctor = false;
          this.spinner.hide();
        }
      });
  }

  save(data) {
    this.router.navigate(["/booking-appointment", data.id]);
  }
}
