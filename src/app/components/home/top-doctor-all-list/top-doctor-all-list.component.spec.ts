import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopDoctorAllListComponent } from './top-doctor-all-list.component';

describe('TopDoctorAllListComponent', () => {
  let component: TopDoctorAllListComponent;
  let fixture: ComponentFixture<TopDoctorAllListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopDoctorAllListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopDoctorAllListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
