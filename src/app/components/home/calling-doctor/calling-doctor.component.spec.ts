import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallingDoctorComponent } from './calling-doctor.component';

describe('CallingDoctorComponent', () => {
  let component: CallingDoctorComponent;
  let fixture: ComponentFixture<CallingDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallingDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallingDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
