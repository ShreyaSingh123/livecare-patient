import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { OpenTokService } from "src/app/shared/services/opentok.service";

@Component({
  selector: "app-calling-doctor",
  templateUrl: "./calling-doctor.component.html",
  styleUrls: ["./calling-doctor.component.css"],
})
export class CallingDoctorComponent implements OnInit {
  appointments: any;
  timeLeft = 30;
  interval;
  session: OT.Session;
  streams: Array<OT.Stream> = [];
  changeDetectorRef: ChangeDetectorRef;
  constructor(
    private toasterService: ToastrService,
    private ref: ChangeDetectorRef,
    private opentokService: OpenTokService
  ) {
    this.changeDetectorRef = ref;
  }

  ngOnInit() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 30;
      }
    }, 1000);
    this.getOpenTokValues();
    debugger;
    this.opentokService
      .initCallSession(
        this.appointments.data.mediaValue.token,
        this.appointments.data.mediaValue.sessionId
      )
      .then((session: OT.Session) => {
        this.session = session;
        this.session.on("streamCreated", (event) => {
          this.streams.push(event.stream);
          this.changeDetectorRef.detectChanges();
        });
        this.session.on("streamDestroyed", (event) => {
          const idx = this.streams.indexOf(event.stream);
          if (idx > -1) {
            this.streams.splice(idx, 1);
            this.changeDetectorRef.detectChanges();
          }
        });
      })
      .then(() => this.opentokService.connect())
      .catch((err) => {
        console.error(err);
        alert(
          "Unable to connect. Make sure you have updated  OpenTok details."
        );
      });
  }

  onTurnOff() {
    this.getOpenTokValues();
    this.opentokService
      .initCallSession(
        this.appointments.data.mediaValue.token,
        this.appointments.data.mediaValue.sessionId
      )
      .then((session: OT.Session) => {
        this.session = session;
        this.session.on("sessionDisconnected", (event) => {
          if (event.reason === "networkDisconnected") {
            this.toasterService.success("Your network connection terminated");
          } else if (event.reason === "clientDisconnected") {
            this.toasterService.success("Your client connection terminated");
          } else {
            this.toasterService.error("connection terminated");
          }
        });
      })
      .then(() => this.opentokService.disconnect())
      .catch((err) => {
        console.error(err);
        alert(
          "Unable to disconnect. Make sure you have updated  OpenTok details."
        );
      });
    // this.router.navigateByUrl("/dashboard/doctor-rating");
  }

  onMute() {
    this.toasterService.success("call is muted");
  }

  getOpenTokValues() {
    this.appointments = JSON.parse(localStorage.getItem("appointments"));
  }
}
