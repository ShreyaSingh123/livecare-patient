import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { environment } from "src/environments/environment";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { Router } from "@angular/router";
import { Filter } from "src/app/shared/interface/filter";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-all-doctor-list",
  templateUrl: "./all-doctor-list.component.html",
  styleUrls: ["./all-doctor-list.component.css"],
})
export class AllDoctorListComponent implements OnInit {
  currentPage: number = 1;
  pageSize: number = 10;
  list: any[];
  rootUrl: any;
  maleImg: any;
  noDoctor: boolean = false;
  doctorSpecialist: any;
  totalCount: any;
  page: number = 1;
  term: string;

  filters: Filter = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
  };

  @ViewChild("backToTop")
  backToTop: ElementRef;

  constructor(
    private doctorList: DoctorListService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.maleImg = environment.maleDmpImg;
    this.rootUrl = environment.baseImgUrl;
    this.getTopDoctorList();
  }
  pageChanged(event: any): void {
    this.filters.pageNumber = event.page;
    this.getTopDoctorList();
    this.backToTop.nativeElement.scrollIntoView({ behavior: "smooth" });
  }

  getTopDoctorList() {
    this.spinner.show();
    this.doctorList.getAllDoctorList(this.filters).subscribe((res) => {
      this.list = res.data.dataList;
      this.totalCount = res.data.totalCount;
      this.doctorSpecialist = res.data.dataList;
      if (res.data.dataList.length == 0) {
        this.noDoctor = true;
        this.spinner.hide();
      } else {
        this.noDoctor = false;
        this.spinner.hide();
      }
    });
  }

  sendData(data) {
    this.router.navigate(["/doctor-info", data.id]);
  }

  save(data) {
    this.router.navigate(["/booking-appointment", data.id]);
  }

  onSearch(event) {
    this.filters.searchQuery = event.target.value;
    this.getTopDoctorList();
  }
}
