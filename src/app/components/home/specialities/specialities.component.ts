import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DoctorListService } from "src/app/shared/services/doctorList.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-specialities",
  templateUrl: "./specialities.component.html",
  styleUrls: ["./specialities.component.css"],
})
export class SpecialitiesComponent implements OnInit {
  specialityList: any;
  rootUrl: any;

  specialitiesPlaceholderImg = environment.specilatiesPlaceholderImg;

  constructor(private doctorList: DoctorListService, private router: Router) {}

  ngOnInit(): void {
    this.rootUrl = environment.baseImgUrl;
    this.doctorList.getSpeciality().subscribe((res) => {
      this.specialityList = res.data.list;
    });
  }

  save(data) {
    this.router.navigate(["/specialitiesDetails", data.id]);
  }
}
