import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { DoctorListService } from "src/app/shared/services/doctorList.service";

import { environment } from "src/environments/environment";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-main-home",
  templateUrl: "./main-home.component.html",
  styleUrls: ["./main-home.component.css"],
})
export class MainHomeComponent implements OnInit {
  profileComplete: any;
  currentPage: number = 1;
  pageSize: number = 10;
  list: any[];
  rootUrl: any;
  maleImg: any;

  noDoctor: boolean = false;
  doctorSpecialist: any;
  doctorId: any;
  showProfile: boolean = false;
  constructor(
    private doctorList: DoctorListService,
    private auth: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    window.history.pushState(null, "", window.location.href);
    window.onpopstate = function () {
      window.history.pushState(null, "", window.location.href);
    };
  }

  ngOnInit() {
    this.maleImg = environment.maleDmpImg;
    this.rootUrl = environment.baseImgUrl;
    this.auth.getPatientDashboardInfo().subscribe((res) => {
      this.profileComplete = res.data.percentageProfileComplete;

      if (this.profileComplete !== 100) {
        this.showProfile = true;
      }
    });
    this.spinner.show();
    this.doctorList
      .getDoctorTopList(this.currentPage, this.pageSize)
      .subscribe((res) => {
        this.list = res.data.dataList;
        this.doctorSpecialist = res.data.dataList;
        if (res.data.dataList.length == 0) {
          this.noDoctor = true;
        } else {
          this.noDoctor = false;
        }
        this.spinner.hide();
      });
  }
  save(data) {
    this.router.navigate(["/booking-appointment", data.id]);
  }
  sendData(data) {
    this.router.navigate(["/doctor-info", data.id]);
  }
}
