import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { NgxSpinner } from "ngx-spinner/lib/ngx-spinner.enum";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-privacy-policy",
  templateUrl: "./privacy-policy.component.html",
  styleUrls: ["./privacy-policy.component.css"],
})
export class PrivacyPolicyComponent implements OnInit {
  Privacy: any;
  constructor(private Auth: AuthService, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.spinner.show();
    this.Auth.getSetting().subscribe((res) => {
      this.Privacy = res.data.privacyPolicy;
      this.spinner.hide();
    });
  }
}
