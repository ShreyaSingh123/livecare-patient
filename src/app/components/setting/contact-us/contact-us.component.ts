import { Component, OnInit } from "@angular/core";

import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { AuthService } from "src/app/shared/services/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-contact-us",
  templateUrl: "./contact-us.component.html",
  styleUrls: ["./contact-us.component.css"],
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  submitted: boolean = false;

  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private toast: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.contactUsForm = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      mobileNo: ["", Validators.required],
      message: ["", Validators.required],
    });
  }

  get f() {
    return this.contactUsForm.controls;
  }
  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.contactUsForm.invalid) {
      this.spinner.hide();
      return;
    }
    this.auth.contactUs(this.contactUsForm.value).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toast.success(res.message);
        this.router.navigateByUrl("/settings");
      } else {
        this.spinner.hide();
        this.toast.error(res.message);
      }
    });
  }
}
