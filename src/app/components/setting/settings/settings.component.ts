import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { LocalizedString } from "@angular/compiler/src/output/output_ast";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.css"],
})
export class SettingsComponent implements OnInit {
  emailNotificationStatus: boolean;
  smsNotificationStatus: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.emailNotificationStatus = currentUser.data.emailNotificationStatus;
    this.smsNotificationStatus = currentUser.data.smsNotificationStatus;
  }

  onLogout() {
    Swal.fire({
      title: "Are you sure you want to sign out?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "No",
      confirmButtonText: "Yes",
      backdrop: `
rgba(0, 0, 0, 0.8)
left top
no-repeat
`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();
        this.authService.logout().subscribe((res) => {
          if (res.status) {
            this.router.navigateByUrl("/login");
            this.spinner.hide();
            this.toaster.success(res.message);
          } else {
            this.spinner.hide();
            this.toaster.error(res.message);
          }
        });
      }
    });
  }
  changedEmail() {
    const data = {
      emailNotificationStatus: this.emailNotificationStatus,
    };

    this.authService.postNotifications(data).subscribe((res) => {});
  }

  onChangedSMS() {
    const sms = {
      smsNotificationStatus: this.smsNotificationStatus,
    };
    this.authService.postNotifications(sms).subscribe((res) => {});
  }
}
