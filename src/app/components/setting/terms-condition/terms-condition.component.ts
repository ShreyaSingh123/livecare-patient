import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-terms-condition",
  templateUrl: "./terms-condition.component.html",
  styleUrls: ["./terms-condition.component.css"],
})
export class TermsConditionComponent implements OnInit {
  Terms: any;
  constructor(private Auth: AuthService, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.spinner.show();
    this.Auth.getSetting().subscribe((res) => {
      this.Terms = res.data.termsConditions;
      this.spinner.hide();
    });
  }
}
