import { Component, OnInit } from "@angular/core";
import { PrescriptionService } from "src/app/shared/services/prescription.service";

@Component({
  selector: "app-prescription",
  templateUrl: "./prescription.component.html",
  styleUrls: ["./prescription.component.css"],
})
export class PrescriptionComponent implements OnInit {
  constructor(private prescriptionService: PrescriptionService) {}

  ngOnInit(): void {
    this.prescriptionService.getPrescriptionList().subscribe((res) => {});
  }
}
