import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-about-us",
  templateUrl: "./about-us.component.html",
  styleUrls: ["./about-us.component.css"],
})
export class AboutUsComponent implements OnInit {
  about: any;
  constructor(private auth: AuthService, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.spinner.show();
    this.auth.getSetting().subscribe((res) => {
      this.about = res.data.aboutUs;
      this.spinner.hide();
    });
  }
}
