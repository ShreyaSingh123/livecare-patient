import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-allergy-history-list",
  templateUrl: "./allergy-history-list.component.html",
  styleUrls: ["./allergy-history-list.component.css"],
})
export class AllergyHistoryListComponent implements OnInit {
  List;
  noAllergyMedicalList: boolean;
  constructor(
    private auth: AuthService,

    private spinner: NgxSpinnerService,
    private toaster: ToastrService
  ) {
    this.spinner.hide();
  }

  ngOnInit() {
    this.getAllergyHistory();
  }
  getAllergyHistory() {
    this.spinner.show();
    this.auth.getAllergyHistoryList().subscribe((res) => {
      this.List = res.data;
      if (res.data.length == 0) {
        this.noAllergyMedicalList = true;
      } else {
        this.noAllergyMedicalList = false;
      }
      this.spinner.hide();
    });
  }

  onDelete(id: any) {
    Swal.fire({
      title: "Are you sure want to delete?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.auth.deleteAllergyList(id).subscribe((res) => {
          this.getAllergyHistory();
          this.spinner.hide();
          this.toaster.success(res.message);
        });
      }
    });
  }
}
