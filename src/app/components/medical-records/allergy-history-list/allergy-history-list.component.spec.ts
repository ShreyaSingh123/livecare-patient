import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllergyHistoryListComponent } from './allergy-history-list.component';

describe('AllergyHistoryListComponent', () => {
  let component: AllergyHistoryListComponent;
  let fixture: ComponentFixture<AllergyHistoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllergyHistoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllergyHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
