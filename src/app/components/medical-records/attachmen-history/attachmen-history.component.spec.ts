import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmenHistoryComponent } from './attachmen-history.component';

describe('AttachmenHistoryComponent', () => {
  let component: AttachmenHistoryComponent;
  let fixture: ComponentFixture<AttachmenHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmenHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmenHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
