import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DatePipe } from "@angular/common";

import { AuthService } from "src/app/shared/services/auth.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-attachmen-history",
  templateUrl: "./attachmen-history.component.html",
  styleUrls: ["./attachmen-history.component.css"],
})
export class AttachmenHistoryComponent implements OnInit {
  _fileData: File = null;
  uploadForm: FormGroup;
  currentDate = new Date();
  maxDate: Date;
  submitted = false;
  constructor(
    private datePipe: DatePipe,
    private auth: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() + 0);
  }

  ngOnInit() {}
  myForm: FormGroup = new FormGroup({
    ReportName: new FormControl("", Validators.required),
    Date: new FormControl("", Validators.required),
    Description: new FormControl("", Validators.required),
    MedicalDocument: new FormControl("", Validators.required),
  });

  changeFile(fileInput: any) {
    this._fileData = <File>fileInput.target.files[0];
    this.onUpdateProfilePic();
  }
  onUpdateProfilePic() {
    const formData = new FormData();
    formData.append("MedicalDocument", this._fileData, this._fileData.name);
  }
  onSubmit() {
    this.submitted = false;
    if (this.myForm.invalid) {
      Swal.fire("Please Complete the entire information");
      return;
    }
    var data = {
      ReportName: this.myForm.controls["ReportName"].value,
      Description: this.myForm.controls["Description"].value,
      Date: this.datePipe.transform(
        this.myForm.controls["Date"].value,
        "dd-MM-yyyy"
      ),
    };
    this.spinner.show();
    this.auth.postAttachment(data, this._fileData).subscribe((res) => {
      this.toastr.success(res.message);
      this.router.navigateByUrl("/attachmentreportslist");
    });
  }
}
