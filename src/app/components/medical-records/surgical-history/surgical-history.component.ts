import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { AuthService } from "src/app/shared/services/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-surgical-history",
  templateUrl: "./surgical-history.component.html",
  styleUrls: ["./surgical-history.component.css"],
})
export class SurgicalHistoryComponent implements OnInit {
  currentDate = new Date();
  maxDate: Date;
  submitted = false;
  constructor(
    private datePipe: DatePipe,
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() + 0);
  }

  ngOnInit(): void {}
  myForm: FormGroup = new FormGroup({
    treatmentName: new FormControl("", Validators.required),
    doctorName: new FormControl("", Validators.required),
    date: new FormControl("", Validators.required),
    description: new FormControl("", Validators.required),
  });

  onSubmit() {
    this.submitted = true;

    if (this.myForm.invalid) {
      Swal.fire("Please Complete the entire information");
      return;
    }
    var data = {
      treatmentName: this.myForm.controls["treatmentName"].value,
      doctorName: this.myForm.controls["doctorName"].value,
      description: this.myForm.controls["description"].value,
      date: this.datePipe.transform(
        this.myForm.controls["date"].value,
        "dd-MM-yyyy"
      ),
    };
    this.spinner.show();
    this.auth.surgicalHistory(data).subscribe((res) => {
      this.toastr.success(res.message);
      this.router.navigateByUrl("/surgicalhistorylist");
    });
  }
}
