import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyHistoryListComponent } from './family-history-list.component';

describe('FamilyHistoryListComponent', () => {
  let component: FamilyHistoryListComponent;
  let fixture: ComponentFixture<FamilyHistoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyHistoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
