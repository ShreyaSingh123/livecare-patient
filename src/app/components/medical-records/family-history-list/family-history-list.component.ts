import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { DatePipe } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-family-history-list",
  templateUrl: "./family-history-list.component.html",
  styleUrls: ["./family-history-list.component.css"],
})
export class FamilyHistoryListComponent implements OnInit {
  List;
  noFamilyHistoryList: boolean;
  constructor(
    private auth: AuthService,

    private spinner: NgxSpinnerService,
    private toaster: ToastrService
  ) {
    this.spinner.hide();
  }

  ngOnInit() {
    this.getFamilyHistory();
  }
  getFamilyHistory() {
    this.spinner.show();
    this.auth.getFamilyHistoryList().subscribe((res) => {
      this.List = res.data;
      if (res.data == 0) {
        this.noFamilyHistoryList = true;
      } else {
        this.noFamilyHistoryList = false;
      }
      this.spinner.hide();
    });
  }

  onDelete(id: any) {
    Swal.fire({
      title: "Are you sure want to delete?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.auth.deleteFamilyList(id).subscribe((res) => {
          this.getFamilyHistory();
          this.spinner.hide();
          this.toaster.success(res.message);
        });
      }
    });
  }
}
