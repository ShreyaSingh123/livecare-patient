import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyMedicalRecordsComponent } from './my-medical-records.component';

describe('MyMedicalRecordsComponent', () => {
  let component: MyMedicalRecordsComponent;
  let fixture: ComponentFixture<MyMedicalRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyMedicalRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyMedicalRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
