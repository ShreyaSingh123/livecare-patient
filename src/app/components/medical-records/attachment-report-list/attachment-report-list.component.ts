import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { environment } from "src/environments/environment";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-attachment-report-list",
  templateUrl: "./attachment-report-list.component.html",
  styleUrls: ["./attachment-report-list.component.css"],
})
export class AttachmentReportListComponent implements OnInit {
  List;

  modalRef: BsModalRef;

  rootUrl: any;
  loadFileUrl: any;
  noAttachmentList: boolean;

  constructor(
    private auth: AuthService,

    private modalService: BsModalService,
    private spinner: NgxSpinnerService,
    private toaster: ToastrService
  ) {
    this.spinner.hide();
  }
  ngOnInit() {
    this.rootUrl = environment.baseImgUrl;
    this.getAttachmentHistoryList();
  }
  getAttachmentHistoryList() {
    this.spinner.show();
    this.auth.getAttachmentHistory().subscribe((res) => {
      this.List = res.data;
      if (res.data.length == 0) {
        this.noAttachmentList = true;
      } else {
        this.noAttachmentList = false;
      }
      this.spinner.hide();
    });
  }

  onDelete(id: any) {
    Swal.fire({
      title: "Are you sure want to delete?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.auth.deleteAttachmentList(id).subscribe((res) => {
          this.getAttachmentHistoryList();
          this.spinner.hide();
          this.toaster.success(res.message);
        });
      }
    });
  }

  openModal(template, passedUrl: any) {
    this.loadFileUrl = this.rootUrl + passedUrl;
    this.modalRef = this.modalService.show(template);
  }
}
