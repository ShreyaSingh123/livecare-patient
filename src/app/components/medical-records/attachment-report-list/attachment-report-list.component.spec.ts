import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentReportListComponent } from './attachment-report-list.component';

describe('AttachmentReportListComponent', () => {
  let component: AttachmentReportListComponent;
  let fixture: ComponentFixture<AttachmentReportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentReportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
