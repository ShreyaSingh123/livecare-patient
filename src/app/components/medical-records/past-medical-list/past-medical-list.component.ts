import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-past-medical-list",
  templateUrl: "./past-medical-list.component.html",
  styleUrls: ["./past-medical-list.component.css"],
})
export class PastMedicalListComponent implements OnInit {
  List: any;
  noPastMedicalList: boolean;
  constructor(
    private auth: AuthService,

    private toaster: ToastrService,

    private spinner: NgxSpinnerService
  ) {
    this.spinner.hide();
  }

  ngOnInit(): void {
    this.getPastMedicalHistoryList();
  }
  getPastMedicalHistoryList() {
    this.spinner.show();
    this.auth.getPastMedicalHistory().subscribe((res) => {
      this.List = res.data;
      if (res.data.length == 0) {
        this.noPastMedicalList = true;
      } else {
        this.noPastMedicalList = false;
      }
      this.spinner.hide();
    });
  }

  onDelete(id: any) {
    Swal.fire({
      title: "Are you sure want to delete?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      this.spinner.show();
      if (result.isConfirmed) {
        this.auth.deletePastMedicalList(id).subscribe((res) => {
          this.getPastMedicalHistoryList();
          this.spinner.hide();
          this.toaster.success(res.message);
        });
      }
    });
  }
}
