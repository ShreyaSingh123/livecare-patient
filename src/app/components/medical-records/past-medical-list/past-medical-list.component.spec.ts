import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastMedicalListComponent } from './past-medical-list.component';

describe('PastMedicalListComponent', () => {
  let component: PastMedicalListComponent;
  let fixture: ComponentFixture<PastMedicalListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastMedicalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastMedicalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
