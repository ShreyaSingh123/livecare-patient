import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-family-history",
  templateUrl: "./family-history.component.html",
  styleUrls: ["./family-history.component.css"],
})
export class FamilyHistoryComponent implements OnInit {
  submitted = false;
  // age(n: number, startFrom: number): number[] {
  //   return [...Array(n).keys()].map(i => i + startFrom);
  // }
  constructor(
    private auth: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}
  myForm: FormGroup = new FormGroup({
    diseaseName: new FormControl("", Validators.required),
    memberName: new FormControl("", Validators.required),
    age: new FormControl("", Validators.required),
    relation: new FormControl("", Validators.required),
    description: new FormControl("", Validators.required),
  });
  onSubmit() {
    this.submitted = true;

    if (this.myForm.invalid) {
      Swal.fire("Please Complete the entire information");
      return;
    }
    var data = {
      diseaseName: this.myForm.get("diseaseName").value,
      memberName: this.myForm.get("memberName").value,
      age: parseInt(this.myForm.get("age").value),
      relation: this.myForm.get("relation").value,
      description: this.myForm.get("description").value,
    };
    this.spinner.show();

    this.auth.postFamilyHistory(data).subscribe((res) => {
      this.toastr.success(res.message);
      this.router.navigateByUrl("/familyhistorylist");
    });
  }
}
