import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "src/app/shared/services/auth.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-allergy-history",
  templateUrl: "./allergy-history.component.html",
  styleUrls: ["./allergy-history.component.css"],
})
export class AllergyHistoryComponent implements OnInit {
  constructor(
    private auth: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {}
  submitted = false;
  ngOnInit(): void {}
  myForm: FormGroup = new FormGroup({
    allergyName: new FormControl("", Validators.required),

    description: new FormControl("", Validators.required),
  });
  onSubmit() {
    this.submitted = true;
    if (this.myForm.invalid) {
      Swal.fire("Please Complete the entire information");
      return;
    }
    this.spinner.show();
    this.auth.postAllergyHistory(this.myForm.value).subscribe((res) => {
      this.toastr.success(res.message);
      this.router.navigateByUrl("/allergyhistorylist");
    });
  }
}
