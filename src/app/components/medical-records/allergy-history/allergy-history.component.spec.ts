import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllergyHistoryComponent } from './allergy-history.component';

describe('AllergyHistoryComponent', () => {
  let component: AllergyHistoryComponent;
  let fixture: ComponentFixture<AllergyHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllergyHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllergyHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
