import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";

import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-surgical-history-list",
  templateUrl: "./surgical-history-list.component.html",
  styleUrls: ["./surgical-history-list.component.css"],
})
export class SurgicalHistoryListComponent implements OnInit {
  List;
  noSurgicalMedicalList: boolean;
  constructor(
    private auth: AuthService,
    private spinner: NgxSpinnerService,
    private toaster: ToastrService
  ) {
    this.spinner.hide();
  }

  ngOnInit(): void {
    this.getSurgicalHistory();
  }

  getSurgicalHistory() {
    this.spinner.show();
    this.auth.getSurgicalHistoryList().subscribe((res) => {
      this.List = res.data;
      if (res.data.length == 0) {
        this.noSurgicalMedicalList = true;
      } else {
        this.noSurgicalMedicalList = false;
      }
      this.spinner.hide();
    });
  }

  onDelete(id: any) {
    Swal.fire({
      title: "Are you sure want to delete?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.auth.deleteSurgicalMedicalList(id).subscribe((res) => {
          this.getSurgicalHistory();
          this.spinner.hide();
          this.toaster.success(res.message);
        });
      }
    });
  }
}
