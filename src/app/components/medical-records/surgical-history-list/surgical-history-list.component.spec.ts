import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgicalHistoryListComponent } from './surgical-history-list.component';

describe('SurgicalHistoryListComponent', () => {
  let component: SurgicalHistoryListComponent;
  let fixture: ComponentFixture<SurgicalHistoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgicalHistoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgicalHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
