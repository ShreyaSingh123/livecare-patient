import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";

import { ConsultServices } from "src/app/shared/services/consult.service";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";

@Component({
  selector: "app-pending-consults",
  templateUrl: "./pending-consults.component.html",
  styleUrls: ["./pending-consults.component.css"],
})
export class PendingConsultsComponent implements OnInit {
  pendingConsult: any = 0;
  upcomingPendingonsults: any;
  rootUrl: any;
  appointmentStatus: any = 3;
  noPendingConsults: boolean;
  listPendingConsults: Array<any> = [];
  constructor(
    private consult: ConsultServices,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toster: ToastrService
  ) {}

  ngOnInit(): void {
    this.rootUrl = environment.baseImgUrl;
    this.upcomingPendingAppointments();
  }
  upcomingPendingAppointments() {
    this.consult.upcomingAppointments(this.pendingConsult).subscribe((res) => {
      this.upcomingPendingonsults = res.data;
      if (this.upcomingPendingonsults == 0) {
        this.noPendingConsults = true;
      } else {
        this.noPendingConsults = false;
      }
      this.listPendingConsults = this.upcomingPendingonsults.map(function (
        user
      ) {
        return {
          appointmentId: user.appointmentId,
          appointmentStatus: user.appointmentStatus,
          date: user.date,
          doctorId: user.doctorId,
          dr_FirstName: user.dr_FirstName,
          dr_LastName: user.dr_LastName,
          dr_ProfilePic: user.dr_ProfilePic,
          patientId: user.patientId,
          paymentStatus: user.paymentStatus,

          slotTo: moment
            .utc(user.slotTo, "DD-MM-YYYY HH:mm:ss")
            .local()
            .format("MM-DD-YYYY HH:mm:ss"),
          slotFrom: moment
            .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
            .local()
            .format("MM-DD-YYYY HH:mm:ss"),
          timeSlotId: user.timeSlotId,
        };
      });
    });
  }

  cancelAppointment(value) {
    var appointmentId = value;
    var data = {
      appointmentId: appointmentId,
      appointmentStatus: parseInt(this.appointmentStatus),
    };

    Swal.fire({
      title: "Appointment Request",
      html: "Are you sure you want to delete this appointment",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.consult.UpdateAppointmentStatus(data).subscribe((res) => {
          this.upcomingPendingAppointments();
          this.spinner.hide();

          if (res.status == true) {
            this.toster.success(res.message);
          } else {
            this.toster.warning(res.message);
          }
        });
      }
    });
  }

  sendData(item) {
    this.router.navigate(["/doctor-info", item.doctorId]);
  }
}
