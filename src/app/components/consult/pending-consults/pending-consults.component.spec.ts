import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingConsultsComponent } from './pending-consults.component';

describe('PendingConsultsComponent', () => {
  let component: PendingConsultsComponent;
  let fixture: ComponentFixture<PendingConsultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingConsultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingConsultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
