import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { ConsultServices } from "src/app/shared/services/consult.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-past-consults",
  templateUrl: "./past-consults.component.html",
  styleUrls: ["./past-consults.component.css"],
})
export class PastConsultsComponent implements OnInit {
  pastConsults: any;
  rootUrl: any;
  pageNumber: any = 1;
  dataForRating: any;
  pastConsultsList: any;
  noPastConsults: boolean;
  pageSize: number = environment.defaultPageSize;

  constructor(
    private consult: ConsultServices,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.rootUrl = environment.baseImgUrl;
    this.pastAppointments();
  }
  pastAppointments() {
    this.spinner.show();
    this.consult
      .pastAppointments(this.pageNumber, this.pageSize)
      .subscribe((res) => {
        this.pastConsults = res.data.dataList;

        if (this.pastConsults == 0) {
          this.noPastConsults = true;
        } else {
          this.noPastConsults = false;
        }
        this.pastConsultsList = this.pastConsults.map(function (user) {
          return {
            appointmentId: user.appointmentId,
            appointmentStatus: user.appointmentStatus,
            date: user.date,
            doctorId: user.doctorId,
            dr_FirstName: user.dr_FirstName,
            dr_LastName: user.dr_LastName,
            dr_ProfilePic: user.dr_ProfilePic,
            patientId: user.patientId,
            paymentStatus: user.paymentStatus,
            doctorSpecialities: user.doctorSpecialities,

            slotTo: moment
              .utc(user.slotTo, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            slotFrom: moment
              .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            timeSlotId: user.timeSlotId,

            timer:
              Math.abs(
                new Date(
                  moment
                    .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
                    .local()
                    .format("YYYY-MM-DDTHH:mm:ss")
                ).getTime() - new Date().getTime()
              ) / 1000,
          };
        });
        this.spinner.hide();
      });
  }

  sendData(item) {
    this.router.navigate(["/doctor-info", item.doctorId]);
  }

  rateNow(item) {
    this.dataForRating = item;
    localStorage.setItem("dataForRating", JSON.stringify(item));
    this.router.navigate(["/user-rating", item.doctorId]);
  }
}
