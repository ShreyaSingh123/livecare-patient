import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastConsultsComponent } from './past-consults.component';

describe('PastConsultsComponent', () => {
  let component: PastConsultsComponent;
  let fixture: ComponentFixture<PastConsultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastConsultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastConsultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
