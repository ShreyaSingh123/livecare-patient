import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmedConsultComponent } from './confirmed-consult.component';

describe('ConfirmedConsultComponent', () => {
  let component: ConfirmedConsultComponent;
  let fixture: ComponentFixture<ConfirmedConsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmedConsultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmedConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
