import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";

import { ConsultServices } from "src/app/shared/services/consult.service";
import { OpenTokService } from "src/app/shared/services/opentok.service";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";

@Component({
  selector: "app-confirmed-consult",
  templateUrl: "./confirmed-consult.component.html",
  styleUrls: ["./confirmed-consult.component.css"],
})
export class ConfirmedConsultComponent implements OnInit {
  confirmedConsult: any = 1;
  upcomingConfirmedConsults: any;
  noConfirmedConsults: boolean;
  rootUrl: any;
  appointmentStatus: any = 3;
  list: Array<any> = [];
  join: boolean;

  AppointmentId: any;

  constructor(
    private consult: ConsultServices,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toster: ToastrService,
    private opentok: OpenTokService
  ) {}

  ngOnInit(): void {
    this.rootUrl = environment.baseImgUrl;
    this.upcomingConfirmedAppointments();
  }

  upcomingConfirmedAppointments() {
    this.spinner.show();
    this.consult
      .upcomingAppointments(this.confirmedConsult)
      .subscribe((res) => {
        this.upcomingConfirmedConsults = res.data;
        if (res.data.length == 0) {
          this.noConfirmedConsults = true;
        } else {
          this.noConfirmedConsults = false;
        }

        this.list = this.upcomingConfirmedConsults.map(function (user) {
          return {
            appointmentId: user.appointmentId,
            appointmentStatus: user.appointmentStatus,
            date: user.date,
            doctorId: user.doctorId,
            dr_FirstName: user.dr_FirstName,
            dr_LastName: user.dr_LastName,
            dr_ProfilePic: user.dr_ProfilePic,
            patientId: user.patientId,
            paymentStatus: user.paymentStatus,
            doctorSpecialities: user.doctorSpecialities,

            slotTo: moment
              .utc(user.slotTo, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            slotFrom: moment
              .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            timeSlotId: user.timeSlotId,

            timer:
              Math.floor(
                new Date(
                  moment
                    .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
                    .local()
                    .format("YYYY-MM-DDTHH:mm:ss")
                ).getTime() - new Date().getTime()
              ) / 1000,
          };
        });

        this.spinner.hide();
      });
  }

  cancelConfirmedAppointment(value) {
    var appointmentId = value;

    var data = {
      appointmentId: appointmentId,
      appointmentStatus: parseInt(this.appointmentStatus),
    };

    Swal.fire({
      title: "Appointment Request",
      html: "Are you sure you want to delete this appointment",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.consult.UpdateAppointmentStatus(data).subscribe((res) => {
          this.upcomingConfirmedAppointments();
          this.spinner.hide();

          if (res.status == true) {
            this.toster.success(res.message);
          } else {
            this.toster.warning(res.message);
          }
        });
      }
    });
  }

  joinCall(data) {
    this.AppointmentId = data.appointmentId;

    this.opentok.getVideoCallToken(this.AppointmentId).subscribe((res) => {
      debugger;

      if (res.status) {
        localStorage.setItem("appointments", JSON.stringify(res));
        this.router.navigateByUrl("/join-call");
      } else {
        return;
      }
    });
  }

  sendData(item) {
    this.router.navigate(["/doctor-info", item.doctorId]);
  }
}
