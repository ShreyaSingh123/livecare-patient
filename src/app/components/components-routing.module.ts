import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "../shared/guards/auth.guard";
import { BlogComponent } from "./main-blog/blog/blog.component";
import { MyMedicalRecordsComponent } from "./medical-records/my-medical-records/my-medical-records.component";

import { PrescriptionComponent } from "./my-prescription/prescription/prescription.component";
import { MyConsultComponent } from "./consult/my-consult/my-consult.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { SettingsComponent } from "./setting/settings/settings.component";
import { AboutUsComponent } from "./about/about-us/about-us.component";
import { MyProfileComponent } from "./my-profile/my-profile.component";
import { BasicComponent } from "./patient-info/basic/basic.component";
import { PastMedicalHistoryComponent } from "./medical-records/past-medical-history/past-medical-history.component";
import { SurgicalHistoryComponent } from "./medical-records/surgical-history/surgical-history.component";
import { AllergyHistoryComponent } from "./medical-records/allergy-history/allergy-history.component";
import { FamilyHistoryComponent } from "./medical-records/family-history/family-history.component";
import { PastMedicalListComponent } from "./medical-records/past-medical-list/past-medical-list.component";
import { AllergyHistoryListComponent } from "./medical-records/allergy-history-list/allergy-history-list.component";
import { SurgicalHistoryListComponent } from "./medical-records/surgical-history-list/surgical-history-list.component";
import { FamilyHistoryListComponent } from "./medical-records/family-history-list/family-history-list.component";
import { AttachmentReportListComponent } from "./medical-records/attachment-report-list/attachment-report-list.component";
import { AttachmenHistoryComponent } from "./medical-records/attachmen-history/attachmen-history.component";
import { ContactUsComponent } from "./setting/contact-us/contact-us.component";
import { PrivacyPolicyComponent } from "./setting/privacy-policy/privacy-policy.component";
import { TermsConditionComponent } from "./setting/terms-condition/terms-condition.component";
import { BlogDetailsComponent } from "./main-blog/blog-details/blog-details.component";
import { TopDoctorAllListComponent } from "./home/top-doctor-all-list/top-doctor-all-list.component";
import { AllDoctorListComponent } from "./home/all-doctor-list/all-doctor-list.component";
import { MainHomeComponent } from "./home/main-home/main-home.component";
import { SpecialitiesAllListComponent } from "./home/specialities-all-list/specialities-all-list.component";
import { SpecialitiesDetailsComponent } from "./home/specialities-details/specialities-details.component";
import { SpecialitiesComponent } from "./home/specialities/specialities.component";
import { DoctorInfoComponent } from "./home/doctor-info/doctor-info.component";
import { DoctorBookingComponent } from "./home/booking/doctor-booking/doctor-booking.component";

import { BookingRequestComponent } from "./home/booking/booking-request/booking-request.component";
import { UserReviewInfoComponent } from "./review/user-review-info/user-review-info.component";
import { UserRatingComponent } from "./review/user-rating/user-rating.component";
import { CallingDoctorComponent } from "./home/calling-doctor/calling-doctor.component";

const routes: Routes = [
  {
    path: "home",
    component: MainHomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "blog",
    component: BlogComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "blog/:id",
    component: BlogDetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "consult",
    component: MyConsultComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "records",
    component: MyMedicalRecordsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "profile",
    component: MyProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "prescription",
    component: PrescriptionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "notifications",
    component: NotificationsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "settings",
    component: SettingsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "about",
    component: AboutUsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "basic",
    component: BasicComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "past-medical",
    component: PastMedicalHistoryComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "pastmedicallist",
    component: PastMedicalListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "surgical-history",
    component: SurgicalHistoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "surgicalhistorylist",
    component: SurgicalHistoryListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "allergy-history",
    component: AllergyHistoryComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "allergyhistorylist",
    component: AllergyHistoryListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "family-history",
    component: FamilyHistoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "familyhistorylist",
    component: FamilyHistoryListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "attachment-reports",
    component: AttachmenHistoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "attachmentreportslist",
    component: AttachmentReportListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "contact-us",
    component: ContactUsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "privacy",
    component: PrivacyPolicyComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "terms",
    component: TermsConditionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "alldoctors",
    component: TopDoctorAllListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "alldoctors/:id",
    component: DoctorInfoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "alldoctorlist",
    component: AllDoctorListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "doctor-info/:id",
    component: DoctorInfoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "allSpecialitiesList",
    component: SpecialitiesAllListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "specialitiesDetails/:id",
    component: SpecialitiesDetailsComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "doctor-info/:id",
    component: DoctorInfoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "specialities",
    component: SpecialitiesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "booking-appointment/:id",
    component: DoctorBookingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "booking-request/:id",
    component: BookingRequestComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "review/:id",
    component: UserReviewInfoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "user-rating/:id",
    component: UserRatingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "join-call",
    component: CallingDoctorComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComponentsRoutingModule {}
