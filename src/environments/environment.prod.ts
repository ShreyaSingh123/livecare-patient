export const environment = {
  production: true,
  apiUrl: "https://livecareapi.azurewebsites.net/api",
  baseImgUrl: "https://livecareapi.azurewebsites.net/",
  defaultPageSize: 10,
  maleDmpImg: "./assets/images/male.jpg",
  specilatiesPlaceholderImg: "./assets/images/specialitiesPlaceholder.jpg",
};
